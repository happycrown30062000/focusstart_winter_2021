package com.nsu.loansapp.screens

import com.nsu.loansapp.R
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.screen.Screen
import io.github.kakaocup.kakao.slider.KSlider
import io.github.kakaocup.kakao.text.KButton

object AddLoanScreen: Screen<AddLoanScreen>() {

    val firstNameField = KEditText { withId(R.id.firs_name_field) }
    val lastNameField = KEditText { withId(R.id.last_name_field) }
    val phoneNumberField = KEditText { withId(R.id.phone_number_field) }
    val amountSlider = KSlider { withId(R.id.amount_slider) }
    val commitButton = KButton { withId(R.id.commit_button) }

}