package com.nsu.loansapp.screens

import android.view.View
import com.nsu.loansapp.R
import io.github.kakaocup.kakao.image.KImageView
import io.github.kakaocup.kakao.recycler.KRecyclerItem
import io.github.kakaocup.kakao.recycler.KRecyclerView
import io.github.kakaocup.kakao.screen.Screen
import io.github.kakaocup.kakao.tabs.KTabLayout
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import org.hamcrest.Matcher

object ListScreen: Screen<ListScreen>() {

    val tabs = KTabLayout { withId(R.id.tabs) }
    val addButton = KButton { withId(R.id.add_button) }

    val loanRecycler = KRecyclerView(
        builder = { withId(R.id.recycler) },
        itemTypeBuilder = { itemType(::LoanItem) }
    )

    class LoanItem(matcher: Matcher<View>): KRecyclerItem<LoanItem>(matcher) {
        val dateText = KTextView(matcher) { withId(R.id.date) }
        val nameText = KTextView(matcher) { withId(R.id.name_text) }
        val amountText = KTextView(matcher) { withId(R.id.amount_text) }
        val statusImage = KImageView(matcher) { withId(R.id.status_image) }
    }
}