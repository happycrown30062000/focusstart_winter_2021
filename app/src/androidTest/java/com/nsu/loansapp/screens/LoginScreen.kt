package com.nsu.loansapp.screens

import com.nsu.loansapp.R
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.screen.Screen
import io.github.kakaocup.kakao.text.KButton

object LoginScreen : Screen<LoginScreen>() {

    val loginEditText = KEditText { withId(R.id.login_field) }
    val passwordEditText = KEditText { withId(R.id.password_field) }
    val loginButton = KButton { withId(R.id.login_button) }
    val registrationButton = KButton { withId(R.id.registration_button) }

}