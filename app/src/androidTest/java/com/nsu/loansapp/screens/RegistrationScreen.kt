package com.nsu.loansapp.screens

import com.nsu.loansapp.R
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.screen.Screen
import io.github.kakaocup.kakao.text.KButton

object RegistrationScreen: Screen<RegistrationScreen>() {

    val registerButton = KButton { withId(R.id.register_button)}
    val backButton = KButton { withId(R.id.back_button) }
    val loginField = KEditText { withId(R.id.login_field) }
    val passwordField = KEditText { withId(R.id.password_field) }
    val repeatPasswordField = KEditText { withId(R.id.repeat_password_field) }

}