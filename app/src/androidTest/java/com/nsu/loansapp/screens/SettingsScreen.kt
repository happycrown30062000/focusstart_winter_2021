package com.nsu.loansapp.screens

import com.nsu.loansapp.R
import io.github.kakaocup.kakao.screen.Screen
import io.github.kakaocup.kakao.text.KButton

object SettingsScreen: Screen<SettingsScreen>() {

    val exitButton = KButton { withId(R.id.exit_button) }

}