package com.nsu.loansapp.tests

import androidx.annotation.DrawableRes
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nsu.loansapp.KTestCase
import com.nsu.loansapp.R
import com.nsu.loansapp.presentation.MainActivity
import com.nsu.loansapp.screens.AddLoanScreen
import com.nsu.loansapp.screens.ListScreen
import com.nsu.loansapp.screens.LoginScreen
import com.nsu.loansapp.screens.SettingsScreen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ListScreenTest : KTestCase() {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    private data class Loan(
        val date: String,
        val amount: String,
        val name: String,
        @DrawableRes val drawable: Int
    )

    private fun checkLoanList(vararg loanList: Loan) {
        loanList.forEachIndexed { index, loan ->
            ListScreen {
                loanRecycler {
                    childAt<ListScreen.LoanItem>(index) {

                        nameText {
                            isDisplayed()
                            hasText(loan.name)
                        }

                        dateText {
                            isDisplayed()
                            hasText(loan.date)
                        }

                        statusImage {
                            isDisplayed()
                            hasDrawable(loan.drawable)
                        }

                        amountText {
                            isDisplayed()
                            containsText(loan.amount)
                        }

                    }
                }
            }
        }
    }

    private fun openListScreen() {
        run {
            step("open list screen") {
                LoginScreen {
                    loginEditText {
                        scrollTo()
                        replaceText("test")
                    }
                    passwordEditText {
                        scrollTo()
                        replaceText("123")
                    }
                    loginButton {
                        scrollTo()
                        click()
                    }
                }
            }
        }
    }

    @Test
    fun openListScreen_expectCorrectList() {
        before {
            openListScreen()
        }.after {
            logOut()
        }.run {
            step("check correct list") {
                ListScreen {
                    tabs {
                        isDisplayed()
                    }
                    loanRecycler {
                        isDisplayed()
                    }
                    checkLoanList(
                        Loan(
                            date = "2021-12-06",
                            amount = "5500",
                            name = "gomer artem",
                            drawable = R.drawable.ic_in_progress
                        ),
                        Loan(
                            date = "2021-12-04",
                            amount = "9500",
                            name = "gomer artem",
                            drawable = R.drawable.ic_error
                        ),
                        Loan(
                            date = "2021-12-04",
                            amount = "9500",
                            name = "gomer artem",
                            drawable = R.drawable.ic_success
                        )
                    )
                }
            }
        }
    }

    @Test
    fun clickAddButton_expectOpenAddLoanScreen() {
        before {
            openListScreen()
        }.after {
            logOut()
        }.run {
            step("click add button") {
                ListScreen {
                    addButton {
                        isDisplayed()
                        click()
                    }
                }
                AddLoanScreen {
                    firstNameField {
                        scrollTo()
                        isDisplayed()
                    }
                    lastNameField {
                        scrollTo()
                        isDisplayed()
                    }
                    phoneNumberField {
                        scrollTo()
                        isDisplayed()
                    }
                    amountSlider {
                        scrollTo()
                        isDisplayed()
                    }
                    commitButton {
                        scrollTo()
                        isDisplayed()
                    }
                    pressBack()
                }
            }
        }
    }

    private fun logOut() {
        ListScreen {
            tabs {
                selectTab(1)
            }
        }
        SettingsScreen {
            exitButton {
                click()
            }
        }
    }

}






