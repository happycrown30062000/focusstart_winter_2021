package com.nsu.loansapp.tests

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nsu.loansapp.KTestCase
import com.nsu.loansapp.R
import com.nsu.loansapp.presentation.MainActivity
import com.nsu.loansapp.screens.ListScreen
import com.nsu.loansapp.screens.LoginScreen
import com.nsu.loansapp.screens.RegistrationScreen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginScreenTest : KTestCase() {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun enterLoginData_expectSuccessLogin() {
        run {
            step(description = "Enter login and password") {
                LoginScreen {

                    loginEditText {
                        scrollTo()
                        typeText("test")
                    }

                    passwordEditText {
                        scrollTo()
                        typeText("123")
                    }

                    loginButton {
                        scrollTo()
                        hasText(R.string.login_text)
                        click()
                    }
                }
            }
            step("check in list screen") {
                ListScreen {
                    tabs {
                        isDisplayed()
                    }
                }
            }
        }
    }

    @Test
    fun clickRegisterButton_expectRegisterScreen() {
        run {
            step("click register button") {
                LoginScreen {
                    registrationButton {
                        scrollTo()
                        click()
                    }
                }
            }
            step("check in register screen") {
                RegistrationScreen {
                    registerButton {
                        scrollTo()
                        isDisplayed()
                    }
                }
            }
        }
    }

}