package com.nsu.loansapp.tests

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nsu.loansapp.KTestCase
import com.nsu.loansapp.presentation.MainActivity
import com.nsu.loansapp.screens.LoginScreen
import com.nsu.loansapp.screens.RegistrationScreen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RegisterScreenTest: KTestCase() {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun enterRegisterData_expectStayInRegisterScreen() {
        run {
            step("go to registration screen") {
                LoginScreen {
                    registrationButton {
                        scrollTo()
                        click()
                    }
                }
            }
            step("enter registration data") {
                RegistrationScreen {
                    loginField {
                        scrollTo()
                        typeText("test")
                    }
                    passwordField {
                        scrollTo()
                        typeText("123")
                    }
                    repeatPasswordField {
                        scrollTo()
                        typeText("123")
                    }
                    registerButton {
                        scrollTo()
                        click()
                        isDisplayed()
                    }
                }
            }
        }
    }

}