package com.nsu.loansapp.data.auth

import com.nsu.loansapp.domain.models.auth.Authorization
import com.nsu.loansapp.domain.models.auth.User
import retrofit2.Response

interface AuthDataSource {

    suspend fun login(authorization: Authorization): Response<String>

    suspend fun register(authorization: Authorization): Response<User>

}