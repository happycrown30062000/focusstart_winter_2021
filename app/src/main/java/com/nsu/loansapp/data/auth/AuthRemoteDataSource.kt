package com.nsu.loansapp.data.auth

import com.nsu.loansapp.domain.models.auth.Authorization
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@BoundTo(AuthDataSource::class, SingletonComponent::class)
class AuthRemoteDataSource @Inject constructor(
    private val authorizationApi: AuthorizationApi
): AuthDataSource {

    override suspend fun login(authorization: Authorization) =
        authorizationApi.login(authorization = authorization)

    override suspend fun register(authorization: Authorization) =
        authorizationApi.register(authorization = authorization)
}