package com.nsu.loansapp.data.auth

import com.nsu.loansapp.domain.models.auth.Authorization
import com.nsu.loansapp.domain.repositories.AuthRepository
import com.nsu.loansapp.utils.toDataStatus
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@BoundTo(AuthRepository::class, SingletonComponent::class)
class AuthRepositoryImpl @Inject constructor(
    private val authDataSource: AuthDataSource
): AuthRepository {

    override suspend fun login(authorization: Authorization) =
        authDataSource.login(authorization = authorization).toDataStatus()


    override suspend fun register(authorization: Authorization) =
        authDataSource.register(authorization = authorization).toDataStatus()
}