package com.nsu.loansapp.data.auth

import com.nsu.loansapp.domain.models.auth.Authorization
import com.nsu.loansapp.domain.models.auth.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthorizationApi {

    companion object {
        const val LOGIN = "login"
        const val REGISTRATION = "registration"
    }

    @POST(LOGIN)
    suspend fun login(@Body authorization: Authorization): Response<String>

    @POST(REGISTRATION)
    suspend fun register(@Body authorization: Authorization): Response<User>
}