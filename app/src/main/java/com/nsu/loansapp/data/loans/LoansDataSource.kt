package com.nsu.loansapp.data.loans

import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.domain.models.loans.LoanConditions
import com.nsu.loansapp.domain.models.loans.LoanRequest
import retrofit2.Response

interface LoansDataSource {

    suspend fun createLoan(loanRequest: LoanRequest): Response<Loan>

    suspend fun getLoan(id: Int): Response<Loan>

    suspend fun getAllLoans(): Response<List<Loan>>

    suspend fun getLoanConditions(): Response<LoanConditions>

}