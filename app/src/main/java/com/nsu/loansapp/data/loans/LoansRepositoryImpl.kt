package com.nsu.loansapp.data.loans

import com.nsu.loansapp.data.loans.local.LoansCacheDataSource
import com.nsu.loansapp.data.loans.local.toLoan
import com.nsu.loansapp.data.loans.local.toLoanEntity
import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.domain.models.loans.LoanRequest
import com.nsu.loansapp.domain.repositories.LoansRepository
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.toDataStatus
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@BoundTo(LoansRepository::class, SingletonComponent::class)
class LoansRepositoryImpl @Inject constructor(
    private val loansDataSource: LoansDataSource,
    private val loansCacheDataSource: LoansCacheDataSource
): LoansRepository {

    override suspend fun saveAllLoans(loans: List<Loan>) {
        loansCacheDataSource.insertAllLoans(loans = loans.map {
            it.toLoanEntity()
        })
    }

    override suspend fun clearCache() {
        loansCacheDataSource.deleteAll()
    }

    override suspend fun createLoan(loanRequest: LoanRequest) =
        loansDataSource.createLoan(loanRequest = loanRequest).toDataStatus()

    override suspend fun getLoan(id: Int) =
        loansDataSource.getLoan(id = id).toDataStatus()

    override suspend fun getAllLoans(isFromCache: Boolean) =
        if (!isFromCache) loansDataSource.getAllLoans().toDataStatus()
        else DataStatus.Success(loansCacheDataSource.getAllLoans().map {
            it.toLoan()
        })

    override suspend fun getLoanConditions() =
        loansDataSource.getLoanConditions().toDataStatus()
}