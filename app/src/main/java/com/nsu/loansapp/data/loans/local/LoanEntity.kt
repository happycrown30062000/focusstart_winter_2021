package com.nsu.loansapp.data.loans.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nsu.loansapp.domain.models.loans.Loan

@Entity(tableName = "Loans")
data class LoanEntity(
    val amount: Int,
    val date: String,
    val firstName: String,
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val lastName: String,
    val percent: Double,
    val period: Int,
    val phoneNumber: String,
    val state: String
)

fun Loan.toLoanEntity(): LoanEntity {
    return LoanEntity(
        amount = this.amount,
        date = this.date,
        firstName = this.firstName,
        id = this.id,
        lastName = this.lastName,
        percent = this.percent,
        period = this.period,
        phoneNumber = this.phoneNumber,
        state = this.state
    )
}

fun LoanEntity.toLoan(): Loan {
    return Loan(
        amount = this.amount,
        date = this.date,
        firstName = this.firstName,
        id = this.id,
        lastName = this.lastName,
        percent = this.percent,
        period = this.period,
        phoneNumber = this.phoneNumber,
        state = this.state
    )
}
