package com.nsu.loansapp.data.loans.local

interface LoansCacheDataSource {

    suspend fun getAllLoans(): List<LoanEntity>

    suspend fun insertAllLoans(loans: List<LoanEntity>)

    suspend fun deleteAll()
}