package com.nsu.loansapp.data.loans.local

import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@BoundTo(LoansCacheDataSource::class, SingletonComponent::class)
class LoansCacheDataSourceImpl @Inject constructor(
    private val loansDao: LoansDao
): LoansCacheDataSource {

    override suspend fun getAllLoans(): List<LoanEntity> =
        loansDao.getAllLoans()

    override suspend fun insertAllLoans(loans: List<LoanEntity>) {
        loansDao.insertAllLoans(loans = loans)
    }

    override suspend fun deleteAll() {
        loansDao.deleteAll()
    }
}