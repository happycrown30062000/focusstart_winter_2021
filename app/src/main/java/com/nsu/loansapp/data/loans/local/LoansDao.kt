package com.nsu.loansapp.data.loans.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface LoansDao {

    @Query("SELECT * FROM Loans ORDER BY date DESC")
    suspend fun getAllLoans(): List<LoanEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllLoans(loans: List<LoanEntity>)

    @Query("DELETE FROM Loans")
    suspend fun deleteAll()

}