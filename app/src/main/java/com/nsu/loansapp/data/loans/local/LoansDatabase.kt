package com.nsu.loansapp.data.loans.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [LoanEntity::class], version = 1, exportSchema = false)
abstract class LoansDatabase: RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "Loans_database"
    }

    abstract val loansDao: LoansDao
}