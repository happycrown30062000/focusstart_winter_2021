package com.nsu.loansapp.data.loans.remote

import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.domain.models.loans.LoanConditions
import com.nsu.loansapp.domain.models.loans.LoanRequest
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface LoansApi {

    companion object {
        const val AUTH_HEADER = "Authorization"
        const val CREATE_LOAN = "loans"
        const val GET_LOAN = "loans/{id}"
        const val GET_ALL_LOANS = "loans/all"
        const val LOAN_CONDITIONS = "loans/conditions"
    }

    @POST(CREATE_LOAN)
    suspend fun createLoan(@Body loanRequest: LoanRequest): Response<Loan>

    @GET(GET_LOAN)
    suspend fun getLoan(@Path("id") id: Int): Response<Loan>

    @GET(GET_ALL_LOANS)
    suspend fun getAllLoans(): Response<List<Loan>>

    @GET(LOAN_CONDITIONS)
    suspend fun getLoanConditions(): Response<LoanConditions>

}