package com.nsu.loansapp.data.loans.remote

import com.nsu.loansapp.data.loans.LoansDataSource
import com.nsu.loansapp.domain.models.loans.LoanRequest
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@BoundTo(LoansDataSource::class, SingletonComponent::class)
class LoansRemoteDataSource @Inject constructor(
    private val loansApi: LoansApi
): LoansDataSource {

    override suspend fun createLoan(loanRequest: LoanRequest) =
        loansApi.createLoan(loanRequest = loanRequest)

    override suspend fun getLoan(id: Int) =
        loansApi.getLoan(id = id)

    override suspend fun getAllLoans() =
        loansApi.getAllLoans()

    override suspend fun getLoanConditions() =
        loansApi.getLoanConditions()
}