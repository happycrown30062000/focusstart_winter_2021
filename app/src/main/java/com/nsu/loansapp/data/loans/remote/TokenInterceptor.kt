package com.nsu.loansapp.data.loans.remote

import com.nsu.loansapp.domain.use_cases.user_info.GetTokenUseCase
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class TokenInterceptor @Inject constructor(
    private val getTokenUseCase: GetTokenUseCase
): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val token = getTokenUseCase()
        val newRequest = request.newBuilder()
            .addHeader(LoansApi.AUTH_HEADER, token)
            .build()
        return chain.proceed(newRequest)
    }
}