package com.nsu.loansapp.data.user_info

interface UserInfoDataSource {

    fun saveToken(token: String)

    fun getToken(): String

    fun deleteToken()

    fun isFirstEnter(): Boolean

    fun setFirstEnter()

    fun isNightMode(): Boolean

    fun setNightMode(isNightMode: Boolean)

    fun setIsRussian(useRussian: Boolean)

    fun getIsRussian(): Boolean
}