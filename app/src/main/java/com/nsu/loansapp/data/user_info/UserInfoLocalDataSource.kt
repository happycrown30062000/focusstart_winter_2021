package com.nsu.loansapp.data.user_info

import android.content.Context
import androidx.core.content.edit
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@BoundTo(UserInfoDataSource::class, SingletonComponent::class)
class UserInfoLocalDataSource @Inject constructor(
    @ApplicationContext
    private val context: Context
): UserInfoDataSource {

    companion object {
        const val SHARED_PREFS_NAME = "UserInfo"
        const val TOKEN_KEY = "token"
        const val FIRST_ENTER_KEY = "firstEnter"
        const val IS_IN_NIGHT_MODE_KEY = "night_mode"
        const val LANGUAGE_KEY = "language"
    }

    private val sharedPreferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)

    override fun saveToken(token: String) {
        sharedPreferences.edit {
            putString(TOKEN_KEY, token)
        }
    }

    override fun getToken(): String {
        return sharedPreferences.getString(TOKEN_KEY, "") ?: ""
    }

    override fun deleteToken() {
        sharedPreferences.edit {
            remove(TOKEN_KEY)
        }
    }

    override fun isFirstEnter(): Boolean {
        return sharedPreferences.getBoolean(FIRST_ENTER_KEY, true)
    }

    override fun setFirstEnter() {
        sharedPreferences.edit {
            putBoolean(FIRST_ENTER_KEY, false)
        }
    }

    override fun isNightMode() =
        sharedPreferences.getBoolean(IS_IN_NIGHT_MODE_KEY, false)


    override fun setNightMode(isNightMode: Boolean) {
        sharedPreferences.edit {
            putBoolean(IS_IN_NIGHT_MODE_KEY, isNightMode)
        }
    }

    override fun setIsRussian(useRussian: Boolean) {
        sharedPreferences.edit {
            putBoolean(LANGUAGE_KEY, useRussian)
        }
    }

    override fun getIsRussian() =
        sharedPreferences.getBoolean(LANGUAGE_KEY, false)

}