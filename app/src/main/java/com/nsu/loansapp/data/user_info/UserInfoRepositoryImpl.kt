package com.nsu.loansapp.data.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import dagger.hilt.components.SingletonComponent
import it.czerwinski.android.hilt.annotations.BoundTo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@BoundTo(UserInfoRepository::class, SingletonComponent::class)
class UserInfoRepositoryImpl @Inject constructor(
    private val userInfoDataSource: UserInfoDataSource
): UserInfoRepository {

    override fun saveToken(token: String) {
        userInfoDataSource.saveToken(token = token)
    }

    override fun getToken() =
        userInfoDataSource.getToken()

    override fun deleteToken() {
        userInfoDataSource.deleteToken()
    }

    override fun isFirstEnter() =
        userInfoDataSource.isFirstEnter()

    override fun setFirstEnter() {
        userInfoDataSource.setFirstEnter()
    }

    override fun isNightMode() =
        userInfoDataSource.isNightMode()

    override fun setNightMode(isNightMode: Boolean) {
        userInfoDataSource.setNightMode(isNightMode = isNightMode)
    }

    override fun setIsRussian(useRussian: Boolean) {
        userInfoDataSource.setIsRussian(useRussian = useRussian)
    }

    override fun getIsRussian() =
        userInfoDataSource.getIsRussian()
}