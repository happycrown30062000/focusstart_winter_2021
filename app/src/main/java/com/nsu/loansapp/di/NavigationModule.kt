package com.nsu.loansapp.di

import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface NavigationModule {

    companion object {
        @Singleton
        @Provides
        fun provideCicerone(): Cicerone<Router> =
            Cicerone.create()

        @Singleton
        @Provides
        fun provideRouter(cicerone: Cicerone<Router>): Router =
            cicerone.router

        @Singleton
        @Provides
        fun provideNavigatorHolder(cicerone: Cicerone<Router>): NavigatorHolder =
            cicerone.getNavigatorHolder()
    }

}