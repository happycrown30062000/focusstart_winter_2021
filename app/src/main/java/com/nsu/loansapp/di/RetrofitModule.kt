package com.nsu.loansapp.di

import com.nsu.loansapp.data.auth.AuthorizationApi
import com.nsu.loansapp.data.loans.remote.LoansApi
import com.nsu.loansapp.data.loans.remote.TokenInterceptor
import com.nsu.loansapp.di.qualifiers.Auth
import com.nsu.loansapp.di.qualifiers.Loans
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RetrofitModule {

    companion object {

        private const val BASE_URL = "https://focusstart.appspot.com/"

        @Singleton
        @Provides
        fun provideOkHttpClientBuilder(): OkHttpClient.Builder {
            return OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
        }

        @Singleton
        @Provides
        fun provideRetrofitBuilder(): Retrofit.Builder {
            return Retrofit.Builder()
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
        }

        @Singleton
        @Provides
        @Auth
        fun provideAuthRetrofit(
            retrofitBuilder: Retrofit.Builder,
            okHttpBuilder: OkHttpClient.Builder
        ): Retrofit {
            return retrofitBuilder
                .client(
                    okHttpBuilder.build()
                )
                .build()
        }

        @Singleton
        @Provides
        @Loans
        fun provideLoansRetrofit(
            retrofitBuilder: Retrofit.Builder,
            okHttpBuilder: OkHttpClient.Builder,
            tokenInterceptor: TokenInterceptor
        ): Retrofit {
            return retrofitBuilder
                .client(
                    okHttpBuilder
                        .addInterceptor(tokenInterceptor)
                        .build()
                ).build()
        }

        @Singleton
        @Provides
        fun provideAuthApi(@Auth retrofit: Retrofit): AuthorizationApi =
            retrofit.create()

        @Singleton
        @Provides
        fun provideLoansApi(@Loans retrofit: Retrofit): LoansApi =
            retrofit.create()
    }

}