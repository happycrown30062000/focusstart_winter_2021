package com.nsu.loansapp.di

import android.content.Context
import androidx.room.Room
import com.nsu.loansapp.data.loans.local.LoansDao
import com.nsu.loansapp.data.loans.local.LoansDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RoomModule {

    companion object {
        @Singleton
        @Provides
        fun provideLoanDatabase(@ApplicationContext context: Context): LoansDatabase {
            return Room.databaseBuilder(
                context,
                LoansDatabase::class.java,
                LoansDatabase.DATABASE_NAME
            )
                .fallbackToDestructiveMigration()
                .build()
        }

        @Singleton
        @Provides
        fun provideLoanDao(database: LoansDatabase): LoansDao {
            return database.loansDao
        }
    }


}