package com.nsu.loansapp.domain.models.auth

data class Authorization(
    val name: String,
    val password: String
)