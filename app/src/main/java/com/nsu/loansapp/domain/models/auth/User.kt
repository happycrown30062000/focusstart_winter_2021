package com.nsu.loansapp.domain.models.auth

data class User(
    val name: String,
    val role: String
)
