package com.nsu.loansapp.domain.models.loans

data class LoanConditions(
    val maxAmount: Int,
    val percent: Double,
    val period: Int
)