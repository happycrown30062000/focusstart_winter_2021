package com.nsu.loansapp.domain.repositories

import com.nsu.loansapp.domain.models.auth.Authorization
import com.nsu.loansapp.domain.models.auth.User
import com.nsu.loansapp.utils.DataStatus

interface AuthRepository {

    suspend fun login(authorization: Authorization): DataStatus<String>

    suspend fun register(authorization: Authorization): DataStatus<User>

}