package com.nsu.loansapp.domain.repositories

import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.domain.models.loans.LoanConditions
import com.nsu.loansapp.domain.models.loans.LoanRequest
import com.nsu.loansapp.utils.DataStatus

interface LoansRepository {

    suspend fun createLoan(loanRequest: LoanRequest): DataStatus<Loan>

    suspend fun getLoan(id: Int): DataStatus<Loan>

    suspend fun getAllLoans(isFromCache: Boolean): DataStatus<List<Loan>>

    suspend fun getLoanConditions(): DataStatus<LoanConditions>

    suspend fun saveAllLoans(loans: List<Loan>)

    suspend fun clearCache()
}