package com.nsu.loansapp.domain.repositories

interface UserInfoRepository {

    fun saveToken(token: String)

    fun getToken(): String

    fun deleteToken()

    fun isFirstEnter(): Boolean

    fun setFirstEnter()

    fun isNightMode(): Boolean

    fun setNightMode(isNightMode: Boolean)

    fun setIsRussian(useRussian: Boolean)

    fun getIsRussian(): Boolean
}