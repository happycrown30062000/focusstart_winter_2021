package com.nsu.loansapp.domain.use_cases.auth

import com.nsu.loansapp.domain.models.auth.Authorization
import com.nsu.loansapp.domain.repositories.AuthRepository
import javax.inject.Inject

class RegisterUseCase @Inject constructor(
    private val authRepository: AuthRepository
) {

    suspend operator fun invoke(authorization: Authorization) =
        authRepository.register(authorization = authorization)

}