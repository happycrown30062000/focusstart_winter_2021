package com.nsu.loansapp.domain.use_cases.loans

import com.nsu.loansapp.domain.models.loans.LoanRequest
import com.nsu.loansapp.domain.repositories.LoansRepository
import javax.inject.Inject

class CreateLoanUseCase @Inject constructor(
    private val loansRepository: LoansRepository
) {

    suspend operator fun invoke(loanRequest: LoanRequest) =
        loansRepository.createLoan(loanRequest = loanRequest)

}