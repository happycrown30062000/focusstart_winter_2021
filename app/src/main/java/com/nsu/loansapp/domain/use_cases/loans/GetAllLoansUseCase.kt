package com.nsu.loansapp.domain.use_cases.loans

import com.nsu.loansapp.domain.repositories.LoansRepository
import javax.inject.Inject

class GetAllLoansUseCase @Inject constructor(
    private val loansRepository: LoansRepository
){

    suspend operator fun invoke(isFromCache: Boolean) =
        loansRepository.getAllLoans(isFromCache = isFromCache)

}