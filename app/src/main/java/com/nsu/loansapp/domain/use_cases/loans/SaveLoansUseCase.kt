package com.nsu.loansapp.domain.use_cases.loans

import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.domain.repositories.LoansRepository
import javax.inject.Inject

class SaveLoansUseCase @Inject constructor(
    private val loansRepository: LoansRepository
){

    suspend operator fun invoke(loans: List<Loan>) {
        loansRepository.saveAllLoans(loans = loans)
    }

}