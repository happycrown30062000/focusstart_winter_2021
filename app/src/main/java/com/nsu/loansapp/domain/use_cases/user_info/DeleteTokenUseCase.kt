package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import javax.inject.Inject

class DeleteTokenUseCase @Inject constructor(
    private val userInfoRepository: UserInfoRepository
){

    operator fun invoke() {
        userInfoRepository.deleteToken()
    }

}