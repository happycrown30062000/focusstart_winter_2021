package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import javax.inject.Inject

class IsNightModeUseCase @Inject constructor(
    private val userInfoRepository: UserInfoRepository
) {

    operator fun invoke() =
        userInfoRepository.isNightMode()

}