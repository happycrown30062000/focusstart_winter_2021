package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import javax.inject.Inject

class SaveTokenUseCase @Inject constructor(
    private val userInfoRepository: UserInfoRepository
) {

    operator fun invoke(token: String) {
        userInfoRepository.saveToken(token = token)
    }

}