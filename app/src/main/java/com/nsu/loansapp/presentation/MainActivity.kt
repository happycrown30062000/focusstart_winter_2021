package com.nsu.loansapp.presentation

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.androidx.AppNavigator
import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.nsu.loansapp.R
import com.nsu.loansapp.data.user_info.UserInfoLocalDataSource
import com.nsu.loansapp.presentation.add_loan.AddLoanFragment
import com.nsu.loansapp.presentation.loan_details.LoanDetailsFragment
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    @Inject
    lateinit var navigatorHolder: NavigatorHolder
    private val navigator = object : AppNavigator(this, R.id.main_container) {
        override fun setupFragmentTransaction(
            screen: FragmentScreen,
            fragmentTransaction: FragmentTransaction,
            currentFragment: Fragment?,
            nextFragment: Fragment
        ) {
            fragmentTransaction.apply {
                if (nextFragment is AddLoanFragment || nextFragment is LoanDetailsFragment) {
                    setCustomAnimations(
                        R.anim.slide_in_from_bottom,
                        R.anim.fade_out,
                        R.anim.fade_in,
                        R.anim.slide_out_to_bottom
                    )
                } else {
                    setCustomAnimations(
                        R.anim.slide_in_from_right,
                        R.anim.slide_out_to_left,
                        R.anim.slide_in_from_left,
                        R.anim.slide_out_to_right
                    )
                }
            }
            super.setupFragmentTransaction(screen, fragmentTransaction, currentFragment, nextFragment)
        }
    }

    private val viewModel by viewModels<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.getIsNightMode()
        initObservers()
    }

    private fun initObservers() {
        viewModel.nightModeState.observe(this) { inNightMode ->
            if (inNightMode) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(applySelectedLanguage(newBase))
    }

    private fun applySelectedLanguage(context: Context): Context {
        val sharedPreferences = context.getSharedPreferences(UserInfoLocalDataSource.SHARED_PREFS_NAME, MODE_PRIVATE)
        val isRussian = sharedPreferences.getBoolean(UserInfoLocalDataSource.LANGUAGE_KEY, false)
        val locale = if (isRussian) Locale("ru") else Locale("en")
        val newConfig = Configuration(context.resources.configuration)
        newConfig.setLocale(locale)
        return context.createConfigurationContext(newConfig)
    }

    private fun applyNewSettings() = recreate()

    override fun onResume() {
        navigatorHolder.setNavigator(navigator)
        getSharedPreferences(UserInfoLocalDataSource.SHARED_PREFS_NAME, MODE_PRIVATE)
            .registerOnSharedPreferenceChangeListener(this)
        super.onResume()
    }

    override fun onPause() {
        getSharedPreferences(UserInfoLocalDataSource.SHARED_PREFS_NAME, MODE_PRIVATE)
            .unregisterOnSharedPreferenceChangeListener(this)
        navigatorHolder.removeNavigator()
        super.onPause()
    }


    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == UserInfoLocalDataSource.IS_IN_NIGHT_MODE_KEY || key == UserInfoLocalDataSource.LANGUAGE_KEY) {
            applyNewSettings()
        }
    }

}