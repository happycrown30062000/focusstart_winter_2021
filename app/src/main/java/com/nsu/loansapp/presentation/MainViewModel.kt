package com.nsu.loansapp.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nsu.loansapp.domain.use_cases.user_info.IsNightModeUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val isNightModeUseCase: IsNightModeUseCase
): ViewModel() {

    private val _nightModeState = MutableLiveData<Boolean>()
    val nightModeState: LiveData<Boolean> = _nightModeState

    fun getIsNightMode() {
        _nightModeState.value = isNightModeUseCase()
    }


}