package com.nsu.loansapp.presentation.add_loan

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.AddLoanFragmentBinding
import com.nsu.loansapp.domain.models.loans.LoanConditions
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.enums.getMessage
import com.nsu.loansapp.utils.shake
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class AddLoanFragment : Fragment(R.layout.add_loan_fragment) {

    private val binding by viewBinding(AddLoanFragmentBinding::bind)
    private val viewModel by viewModels<AddLoanViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initObservers()
        initListeners()
        viewModel.loadConditions()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initListeners() {
        with(binding) {
            commitButton.setOnClickListener {
                viewModel.validateFields(
                    firstName = firsNameField.text?.toString(),
                    lastName = lastNameField.text?.toString(),
                    phoneNumber = phoneNumberField.text?.toString(),
                    amount = amountValue.text?.toString(),
                )
            }
        }
    }

    private fun initObservers() {
        viewModel.conditionState.observe(viewLifecycleOwner) { status ->
            when (status) {
                is DataStatus.Loading -> renderLoadingState()
                is DataStatus.Success -> renderSuccessLoadingState(status = status)
                is DataStatus.Error -> renderErrorState(status = status)
            }
        }

        viewModel.createState.observe(viewLifecycleOwner) { status ->
            when (status) {
                is DataStatus.Loading -> renderLoadingState()
                is DataStatus.Success -> renderSuccessCreateState()
                is DataStatus.Error -> renderErrorState(status = status)
            }
        }

        viewModel.fieldsState.observe(viewLifecycleOwner) { isValid ->
            with(binding) {
                if (isValid) {
                    viewModel.createLoan(
                        firstName = firsNameField.text.toString(),
                        lastName = lastNameField.text.toString(),
                        phoneNumber = phoneNumberField.text.toString(),
                        amount = amountValue.text.toString().toDouble().toInt()
                    )
                } else {
                    showMessage(getString(R.string.empty_fields))
                    firsNameFieldLayout.shake(2, 300).start()
                    lastNameFieldLayout.shake(2, 300).start()
                    phoneNumberFieldLayout.shake(2, 300).start()
                    amountSlider.shake(2, 300).start()
                }
            }
        }
    }

    private fun renderSuccessCreateState() {
        with(binding) {
            conditionCard.card.visibility = View.VISIBLE
            fields.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
        ResultDialogFragment.newInstance(
            isSuccess = true
        ).show(childFragmentManager, null)
    }

    private fun renderSuccessLoadingState(status: DataStatus.Success<LoanConditions>) {
        with(binding) {
            conditionCard.card.visibility = View.VISIBLE
            fields.visibility = View.VISIBLE
            progressBar.visibility = View.GONE

            val conditions = status.data
            conditionCard.maxAmountValue.text = requireContext().getString(R.string.money_format, conditions.maxAmount)
            conditionCard.periodValue.text = requireContext().getString(R.string.period_format, conditions.period)
            conditionCard.percentValue.text = requireContext().getString(R.string.percent_format, conditions.percent)

            amountSlider.valueTo = conditions.maxAmount.toFloat()
            amountSlider.setLabelFormatter { value: Float ->
                val currency = Currency.getInstance("RUB")
                "${currency.symbol} $value"
            }
            amountSlider.addOnChangeListener { _, value, _ ->
                amountValue.text = value.toString()
            }
            amountValue.text = amountSlider.value.toString()
        }
    }

    private fun renderErrorState(status: DataStatus.Error) {
        with(binding) {
            conditionCard.card.visibility = View.VISIBLE
            fields.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
        val message = status.errorType.getMessage(requireContext())
        showMessage(message = message)
        ResultDialogFragment.newInstance(
            isSuccess = false,
            reason = message
        ).show(childFragmentManager, null)
    }

    private fun renderLoadingState() {
        with(binding) {
            conditionCard.card.visibility = View.GONE
            fields.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
    }

}