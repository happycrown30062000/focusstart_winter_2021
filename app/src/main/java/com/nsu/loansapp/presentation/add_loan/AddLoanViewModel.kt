package com.nsu.loansapp.presentation.add_loan

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.domain.models.loans.LoanConditions
import com.nsu.loansapp.domain.models.loans.LoanRequest
import com.nsu.loansapp.domain.use_cases.loans.CreateLoanUseCase
import com.nsu.loansapp.domain.use_cases.loans.GetLoanConditionsUseCase
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.events.SingleLiveEvent
import com.nsu.loansapp.utils.getErrorType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddLoanViewModel @Inject constructor(
    private val getLoanConditionsUseCase: GetLoanConditionsUseCase,
    private val createLoanUseCase: CreateLoanUseCase
): ViewModel() {

    private val _conditionsState = MutableLiveData<DataStatus<LoanConditions>>()
    val conditionState: LiveData<DataStatus<LoanConditions>> = _conditionsState

    private val _createState = MutableLiveData<DataStatus<Loan>>()
    val createState: LiveData<DataStatus<Loan>> = _createState

    private val _fieldsState = SingleLiveEvent<Boolean>()
    val fieldsState: LiveData<Boolean> = _fieldsState

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        val errorType = throwable.getErrorType()
        _conditionsState.value = DataStatus.Error(errorType = errorType)
    }

    fun loadConditions() {
        viewModelScope.launch(exceptionHandler) {
            _conditionsState.value = DataStatus.Loading
            val state = getLoanConditionsUseCase()
            _conditionsState.value = state
        }
    }

    fun createLoan(
        firstName: String,
        lastName: String,
        phoneNumber: String,
        amount: Int
    ) {
        viewModelScope.launch(exceptionHandler) {
            _createState.value = DataStatus.Loading
            val loanRequest = LoanRequest(
                firstName = firstName,
                lastName = lastName,
                amount = amount,
                phoneNumber = phoneNumber,
                percent = (_conditionsState.value as DataStatus.Success).data.percent,
                period =  (_conditionsState.value as DataStatus.Success).data.period
            )
            val state = createLoanUseCase(loanRequest = loanRequest)
            _createState.value = state
        }
    }

    fun validateFields(
        firstName: String?,
        lastName: String?,
        amount: String?,
        phoneNumber: String?
    ) {
       _fieldsState.value = !firstName.isNullOrBlank()
               && !lastName.isNullOrBlank()
               && !amount.isNullOrBlank()
               && !phoneNumber.isNullOrBlank()
    }

}