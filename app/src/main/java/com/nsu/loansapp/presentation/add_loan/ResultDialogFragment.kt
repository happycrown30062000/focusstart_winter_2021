package com.nsu.loansapp.presentation.add_loan

import android.os.Bundle
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.FragmentResultDialogBinding

class ResultDialogFragment private constructor(): DialogFragment(R.layout.fragment_result_dialog) {

    companion object {
        private const val IS_SUCCESS_KEY = "isSuccess"
        private const val REASON_KEY = "reason"

        fun newInstance(isSuccess: Boolean, reason: String? = null): ResultDialogFragment {
            val fragment = ResultDialogFragment()
            val args = bundleOf(
                IS_SUCCESS_KEY to isSuccess,
                REASON_KEY to reason
            )
            fragment.arguments = args
            return fragment
        }
    }

    private val binding by viewBinding(FragmentResultDialogBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val isSuccess = requireArguments().getBoolean(IS_SUCCESS_KEY)
        with(binding) {
            if (isSuccess) {
                resultIcon.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.ic_success))
                resultText.text = getString(R.string.add_success)
            } else {
                resultIcon.setImageDrawable(AppCompatResources.getDrawable(requireContext(), R.drawable.ic_error))
                resultText.text = getString(R.string.add_error)
                reasonText.visibility = View.VISIBLE
                reasonText.text = requireArguments().getString(REASON_KEY)
            }
            okButton.setOnClickListener {
                dismiss()
            }
        }

        super.onViewCreated(view, savedInstanceState)
    }

}