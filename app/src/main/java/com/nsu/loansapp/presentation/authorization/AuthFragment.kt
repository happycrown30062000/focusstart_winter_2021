package com.nsu.loansapp.presentation.authorization

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.FragmentAuthBinding
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.closeSoftKeyboard
import com.nsu.loansapp.utils.enums.getMessage
import com.nsu.loansapp.utils.shake
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthFragment : Fragment(R.layout.fragment_auth) {

    private val binding by viewBinding(FragmentAuthBinding::bind)
    private val viewModel by viewModels<AuthViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initObservers()
        initListeners()
        viewModel.isAuthorized()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initListeners() {
        with(binding) {
            loginButton.setOnClickListener {
                it.closeSoftKeyboard()
                viewModel.validateFields(
                    login = loginField.text?.toString(),
                    password = passwordField.text?.toString()
                )
            }

            registrationButton.setOnClickListener {
                viewModel.openRegistrationScreen()
            }
        }
    }

    private fun initObservers() {
        viewModel.loginState.observe(viewLifecycleOwner) { status ->
            when (status) {
                is DataStatus.Loading -> renderLoadingState()
                is DataStatus.Success -> renderSuccessState(status = status)
                is DataStatus.Error -> renderErrorState(status = status)
            }
        }

        viewModel.fieldsState.observe(viewLifecycleOwner) { isValid ->
            with(binding) {
                if (isValid) {
                    viewModel.login(
                        login = loginField.text.toString().trim(),
                        password = passwordField.text.toString().trim()
                    )
                } else {
                    loginFieldLayout.shake(2, 300).start()
                    passwordFieldLayout.shake(2, 300).start()
                    showMessage(getString(R.string.empty_fields))
                }
            }

        }

        viewModel.authState.observe(viewLifecycleOwner) { isAuthorized ->
            if (isAuthorized) {
                viewModel.openMainScreen()
            }
        }
    }

    private fun renderErrorState(status: DataStatus.Error) {
        with(binding) {
            contentContainer.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
        val message = status.errorType.getMessage(requireContext())
        showMessage(message = message)
    }

    private fun renderSuccessState(status: DataStatus.Success<String>) {
        viewModel.saveToken(token = status.data)
        viewModel.openFirstScreen()
    }

    private fun renderLoadingState() {
        with(binding) {
            contentContainer.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
    }

}