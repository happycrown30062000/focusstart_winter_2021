package com.nsu.loansapp.presentation.authorization

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.terrakok.cicerone.Router
import com.nsu.loansapp.domain.models.auth.Authorization
import com.nsu.loansapp.domain.use_cases.auth.LoginUseCase
import com.nsu.loansapp.domain.use_cases.user_info.GetTokenUseCase
import com.nsu.loansapp.domain.use_cases.user_info.IsFirstEnterUseCase
import com.nsu.loansapp.domain.use_cases.user_info.SaveTokenUseCase
import com.nsu.loansapp.domain.use_cases.user_info.SetFirstEnterUseCase
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.events.SingleLiveEvent
import com.nsu.loansapp.utils.getErrorType
import com.nsu.loansapp.utils.navigation.Screens
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val loginUseCase: LoginUseCase,
    private val saveTokenUseCase: SaveTokenUseCase,
    private val isFirstEnterUseCase: IsFirstEnterUseCase,
    private val setFirstEnterUseCase: SetFirstEnterUseCase,
    private val getTokenUseCase: GetTokenUseCase,
    private val router: Router
) : ViewModel() {

    private val _authState = SingleLiveEvent<Boolean>()
    val authState: LiveData<Boolean> = _authState

    private val _loginState = SingleLiveEvent<DataStatus<String>>()
    val loginState: LiveData<DataStatus<String>> = _loginState

    private val _fieldsState = SingleLiveEvent<Boolean>()
    val fieldsState: LiveData<Boolean> = _fieldsState

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        val errorType = throwable.getErrorType()
        _loginState.value = DataStatus.Error(errorType = errorType)
    }

    fun login(login: String, password: String) {
        viewModelScope.launch(exceptionHandler) {
            _loginState.value = DataStatus.Loading
            val state = loginUseCase(
                Authorization(
                    name = login,
                    password = password
                )
            )
            _loginState.value = state
        }
    }

    fun isAuthorized() {
        viewModelScope.launch {
        val token = getTokenUseCase()
            _authState.value = token.isNotBlank()
        }
    }

    fun saveToken(token: String) {
        viewModelScope.launch(exceptionHandler) {
            saveTokenUseCase(token = token)
        }
    }

    private fun isFirstEnter() = isFirstEnterUseCase()

    private fun setFirstEnter() {
        setFirstEnterUseCase()
    }

    fun openFirstScreen() {
        if (isFirstEnter()) {
            setFirstEnter()
            openOnBoardingScreen()
        } else {
            openMainScreen()
        }
    }

    fun openMainScreen() {
        router.newRootScreen(Screens.MainScreen())
    }

    private fun openOnBoardingScreen() {
        router.newRootScreen(Screens.OnBoardingScreen())
    }

    fun openRegistrationScreen() {
        router.navigateTo(Screens.RegistrationScreen())
    }

    fun validateFields(login: String?, password: String?) {
        _fieldsState.value = !login.isNullOrBlank() && !password.isNullOrBlank()
    }


}