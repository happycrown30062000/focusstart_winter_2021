package com.nsu.loansapp.presentation.loan_details

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.LoanDetailsFragmentBinding
import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.presentation.loans_list.adapter.LoanViewHolder
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.enums.getMessage
import com.nsu.loansapp.utils.toDateString
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoanDetailsFragment: Fragment(R.layout.loan_details_fragment) {

    companion object {

        private const val ID_KEY = "id"

        fun newInstance(id: Int): LoanDetailsFragment {
            val fragment = LoanDetailsFragment()
            val args = bundleOf(
                ID_KEY to id
            )
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var viewModelFactory: LoanDetailsViewModel.Factory.Factory

    private val viewModel by viewModels<LoanDetailsViewModel> {
        viewModelFactory.create(requireArguments().getInt(ID_KEY))
    }
    private val binding by viewBinding(LoanDetailsFragmentBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initObservers()
        initListeners()
        viewModel.loadDetails()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initListeners() {
        binding.okButton.setOnClickListener {
            viewModel.navigateBack()
        }
    }

    private fun initObservers() {
        viewModel.detailsState.observe(viewLifecycleOwner) { status ->
            when (status) {
                is DataStatus.Loading -> renderLoadingState()
                is DataStatus.Success -> renderSuccessState(status = status)
                is DataStatus.Error -> renderErrorState(status = status)
            }
        }
    }

    private fun renderSuccessState(status: DataStatus.Success<Loan>) {
        with(binding) {

            progressBar.visibility = View.GONE
            detailsCard.detailsCard.visibility = View.VISIBLE
            okButton.visibility = View.VISIBLE

            val data = status.data
            with(detailsCard) {
                date.text = data.date.toDateString(viewModel.getIsRussian())
                this.status.text = data.state
                name.text = getString(R.string.full_name_format, data.firstName, data.lastName)
                period.text = getString(R.string.full_period_format, data.period)
                percent.text = getString(R.string.full_percent_format, data.percent)
                phone.text = getString(R.string.phone_number_format, data.phoneNumber)
                amount.text = getString(R.string.amount_format, data.amount)

                val color = when(data.state) {
                    LoanViewHolder.APPROVED_STATE -> ContextCompat.getColor(requireContext(), R.color.green)
                    LoanViewHolder.REGISTERED_STATE -> ContextCompat.getColor(requireContext(), R.color.orange)
                    LoanViewHolder.REJECTED_STATE -> ContextCompat.getColor(requireContext(), R.color.red)
                    else -> error("Unknown state")
                }
                this.status.setTextColor(color)
            }
            if (data.state == LoanViewHolder.APPROVED_STATE) {
                infoText.visibility = View.VISIBLE
            }
        }
    }

    private fun renderErrorState(status: DataStatus.Error) {
        val message = status.errorType.getMessage(requireContext())
        showMessage(message = message)
        viewModel.navigateBack()
    }

    private fun renderLoadingState() {
        with(binding) {
            progressBar.visibility = View.VISIBLE
            detailsCard.detailsCard.visibility = View.GONE
            okButton.visibility = View.GONE
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
    }

}