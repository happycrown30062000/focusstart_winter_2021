package com.nsu.loansapp.presentation.loan_details

import androidx.lifecycle.*
import com.github.terrakok.cicerone.Router
import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.domain.use_cases.loans.GetLoanUseCase
import com.nsu.loansapp.domain.use_cases.user_info.GetIsRussianUseCase
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.getErrorType
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch

class LoanDetailsViewModel(
    private val id: Int,
    private val getLoanUseCase: GetLoanUseCase,
    private val getIsRussianUseCase: GetIsRussianUseCase,
    private val router: Router
) : ViewModel() {

    private val _detailsState = MutableLiveData<DataStatus<Loan>>()
    val detailsState: LiveData<DataStatus<Loan>> = _detailsState

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        val errorType = throwable.getErrorType()
        _detailsState.value = DataStatus.Error(errorType = errorType)
    }

    fun loadDetails() {
        viewModelScope.launch(exceptionHandler) {
            _detailsState.value = DataStatus.Loading
            val state = getLoanUseCase(id = id)
            _detailsState.value = state
        }
    }

    fun getIsRussian() = getIsRussianUseCase()

    fun navigateBack() {
        router.exit()
    }

    class Factory @AssistedInject constructor(
        @Assisted("id") private val id: Int,
        private val getLoanUseCase: GetLoanUseCase,
        private val getIsRussianUseCase: GetIsRussianUseCase,
        private val router: Router
    ): ViewModelProvider.Factory {

        @Suppress("Unchecked_cast")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            require(modelClass == LoanDetailsViewModel::class.java)
            return LoanDetailsViewModel(
                id = id,
                getLoanUseCase = getLoanUseCase,
                router = router,
                getIsRussianUseCase = getIsRussianUseCase
            ) as T
        }

        @AssistedFactory
        interface Factory {

            fun create(
                @Assisted("id") id: Int
            ): LoanDetailsViewModel.Factory

        }

    }

}

