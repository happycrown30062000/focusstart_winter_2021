package com.nsu.loansapp.presentation.loans_list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.FragmentLoansListBinding
import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.presentation.loans_list.adapter.LoansListAdapter
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.enums.getMessage
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoansListFragment : Fragment(R.layout.fragment_loans_list) {

    private val viewModel by viewModels<LoansListViewModel>()
    private val binding by viewBinding(FragmentLoansListBinding::bind)
    private val adapter by lazy {
        LoansListAdapter(
            onItemClick = {
                viewModel.openLoanDetailsScreen(id = it)
            },
            isRussian = viewModel.getIsRussian()
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initObservers()
        initListeners()
        initRecycler()
        viewModel.getCachedData()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initListeners() {
        with(binding) {
            addButton.setOnClickListener {
                viewModel.navigateToAddLoanScreen()
            }

            refreshLayout.setOnRefreshListener {
                viewModel.getRemoteData()
            }
        }
    }

    private fun initRecycler() {
        with(binding) {
            recycler.adapter = adapter
        }
    }

    private fun initObservers() {
        viewModel.listState.observe(viewLifecycleOwner) { status ->
            when (status) {
                is DataStatus.Loading -> renderLoadingState()
                is DataStatus.Success -> renderSuccessState(status = status)
                is DataStatus.Error -> renderErrorState(status = status)
            }
        }
    }

    private fun renderErrorState(status: DataStatus.Error) {
        with(binding) {
            recycler.visibility = View.VISIBLE
            refreshLayout.isRefreshing = false
        }
        val message = status.errorType.getMessage(requireContext())
        showMessage(message = message)
    }

    private fun renderSuccessState(status: DataStatus.Success<List<Loan>>) {
        with(binding) {
            refreshLayout.isRefreshing = false
            adapter.submitList(status.data)
            if (status.data.isEmpty()) {
                emptyListText.visibility = View.VISIBLE
                recycler.visibility = View.GONE
            } else {
                emptyListText.visibility = View.GONE
                recycler.visibility = View.VISIBLE
            }
        }
    }

    private fun renderLoadingState() {
        with(binding) {
            refreshLayout.isRefreshing = true
            recycler.visibility = View.GONE
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
    }
}