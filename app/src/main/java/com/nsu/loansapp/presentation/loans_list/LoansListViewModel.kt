package com.nsu.loansapp.presentation.loans_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.terrakok.cicerone.Router
import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.domain.use_cases.loans.GetAllLoansUseCase
import com.nsu.loansapp.domain.use_cases.loans.SaveLoansUseCase
import com.nsu.loansapp.domain.use_cases.user_info.GetIsRussianUseCase
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.getErrorType
import com.nsu.loansapp.utils.navigation.Screens
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoansListViewModel @Inject constructor(
    private val getAllLoansUseCase: GetAllLoansUseCase,
    private val getIsRussianUseCase: GetIsRussianUseCase,
    private val saveLoansUseCase: SaveLoansUseCase,
    private val router: Router
): ViewModel() {

    private val _listState = MutableLiveData<DataStatus<List<Loan>>>()
    val listState: LiveData<DataStatus<List<Loan>>> = _listState

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        val errorType = throwable.getErrorType()
        _listState.value = DataStatus.Error(errorType = errorType)
    }

    fun getCachedData() {
        viewModelScope.launch(exceptionHandler) {
            _listState.value = DataStatus.Loading
            val cacheStatus = getAllLoansUseCase(isFromCache = true)
            if (cacheStatus is DataStatus.Success) {
                if (cacheStatus.data.isEmpty()) {
                    getRemoteData()
                } else {
                    _listState.value = cacheStatus
                }
            } else {
                getRemoteData()
            }
        }
    }

    fun getRemoteData() {
        viewModelScope.launch(exceptionHandler) {
            _listState.value = DataStatus.Loading
            val loadingStatus = getAllLoansUseCase(isFromCache = false)
            if (loadingStatus is DataStatus.Success) {
                saveLoansUseCase(loadingStatus.data)
            }
            _listState.value = loadingStatus
        }
    }

    fun navigateToAddLoanScreen() {
        router.navigateTo(Screens.AddLoanScreen())
    }

    fun openLoanDetailsScreen(id: Int) {
        router.navigateTo(Screens.LoanDetailsScreen(id = id))
    }

    fun getIsRussian() = getIsRussianUseCase()

}