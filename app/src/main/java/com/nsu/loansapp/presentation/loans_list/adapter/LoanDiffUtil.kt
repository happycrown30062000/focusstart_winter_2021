package com.nsu.loansapp.presentation.loans_list.adapter

import androidx.recyclerview.widget.DiffUtil
import com.nsu.loansapp.domain.models.loans.Loan

object LoanDiffUtil: DiffUtil.ItemCallback<Loan>() {
    override fun areItemsTheSame(oldItem: Loan, newItem: Loan): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Loan, newItem: Loan): Boolean {
        return oldItem.firstName == newItem.firstName
                && oldItem.lastName == newItem.lastName
                && oldItem.amount == newItem.amount
                && oldItem.state == newItem.state
                && oldItem.date == newItem.date
    }
}