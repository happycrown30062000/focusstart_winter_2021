package com.nsu.loansapp.presentation.loans_list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.LoanListItemBinding
import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.utils.toDateString

private const val TAG = "LoanViewHolder"

class LoanViewHolder private constructor(
    private val binding: LoanListItemBinding,
    private val onItemClick: (Int) -> Unit,
    private val isRussian: Boolean
): RecyclerView.ViewHolder(binding.root) {

    companion object {

        const val APPROVED_STATE = "APPROVED"
        const val REJECTED_STATE = "REJECTED"
        const val REGISTERED_STATE = "REGISTERED"

        operator fun invoke(parent: ViewGroup, onItemClick: (Int) -> Unit, isRussian: Boolean): LoanViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = LoanListItemBinding.inflate(inflater, parent, false)
            return LoanViewHolder(binding = binding, onItemClick = onItemClick, isRussian = isRussian)
        }
    }

    fun bind(loan: Loan) {
        with(binding) {
            root.setOnClickListener {
                onItemClick(loan.id)
            }
            date.text = loan.date.toDateString(isRussian)
            nameText.text = itemView.context.getString(R.string.name_format, loan.lastName, loan.firstName)
            val context = itemView.context
            when(loan.state) {
                APPROVED_STATE -> {
                    statusImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_success))
                }
                REJECTED_STATE -> {
                    statusImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_error))
                }
                REGISTERED_STATE -> {
                    statusImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_in_progress))
                }
                else -> {
                    error("Unknown state")
                }
            }
            amountText.text = itemView.context.getString(R.string.money_format, loan.amount)
        }
    }



}