package com.nsu.loansapp.presentation.loans_list.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.nsu.loansapp.domain.models.loans.Loan

class LoansListAdapter(
    private val onItemClick: (Int) -> Unit,
    private val isRussian: Boolean
): ListAdapter<Loan, LoanViewHolder>(LoanDiffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LoanViewHolder {
        return LoanViewHolder(
            parent = parent,
            onItemClick = onItemClick,
            isRussian = isRussian
        )
    }

    override fun onBindViewHolder(holder: LoanViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}