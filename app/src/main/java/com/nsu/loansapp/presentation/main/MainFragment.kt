package com.nsu.loansapp.presentation.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.tabs.TabLayoutMediator
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.FragmentMainBinding

class MainFragment: Fragment(R.layout.fragment_main) {

    private val binding by viewBinding(FragmentMainBinding::bind)

    private fun initViewPager() {
        with(binding) {
            viewPager.adapter = MainScreenAdapter(
                fragmentManager = childFragmentManager,
                lifecycle = viewLifecycleOwner.lifecycle
            )
            TabLayoutMediator(tabs, viewPager) { tab, position ->
                val text = when(position) {
                    0 -> getString(R.string.loans_list)
                    1 -> getString(R.string.settings)
                    else -> error("Unknown position")
                }
                tab.text = text
            }.attach()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViewPager()
        super.onViewCreated(view, savedInstanceState)
    }


}