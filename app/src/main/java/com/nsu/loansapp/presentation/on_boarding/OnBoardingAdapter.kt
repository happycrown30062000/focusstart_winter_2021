package com.nsu.loansapp.presentation.on_boarding

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

class OnBoardingAdapter(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle,
    private val items: List<SlideItem>
): FragmentStateAdapter(fragmentManager, lifecycle) {


    override fun getItemCount() = 3

    override fun createFragment(position: Int) =
        SlideFragment.newInstance(items[position])
}