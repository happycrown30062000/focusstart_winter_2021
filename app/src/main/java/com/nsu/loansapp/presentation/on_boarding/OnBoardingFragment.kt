package com.nsu.loansapp.presentation.on_boarding

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.tabs.TabLayoutMediator
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.FragmentOnBoardingBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnBoardingFragment: Fragment(R.layout.fragment_on_boarding) {

    private val binding by viewBinding(FragmentOnBoardingBinding::bind)
    private val viewModel by viewModels<OnBoardingViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViewPager()
        initListeners()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initListeners() {
        binding.skipButton.setOnClickListener {
            viewModel.navigateToLoansList()
        }
    }

    private fun initViewPager() {
        with(binding) {
            val adapter = OnBoardingAdapter(
                fragmentManager = childFragmentManager,
                lifecycle = viewLifecycleOwner.lifecycle,
                items = listOf(
                    SlideItem(
                        drawable = R.drawable.ic_loan_list,
                        text = getString(R.string.slide_list)
                    ),
                    SlideItem(
                        drawable = R.drawable.ic_loan_details,
                        text = getString(R.string.slide_details)
                    ),
                    SlideItem(
                        drawable = R.drawable.ic_money,
                        text = getString(R.string.slide_money)
                    ),
                )
            )
            viewPager.adapter = adapter
            TabLayoutMediator(tabs, viewPager) { tab, position ->
                tab.text = getString(R.string.page_format, (position + 1).toString())
            }.attach()
        }
    }

}