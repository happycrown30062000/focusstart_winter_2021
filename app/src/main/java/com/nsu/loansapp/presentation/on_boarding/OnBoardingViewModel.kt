package com.nsu.loansapp.presentation.on_boarding

import androidx.lifecycle.ViewModel
import com.github.terrakok.cicerone.Router
import com.nsu.loansapp.utils.navigation.Screens
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OnBoardingViewModel @Inject constructor(
    private val router: Router
): ViewModel() {

    fun navigateToLoansList() {
        router.newRootScreen(Screens.MainScreen())
    }

}