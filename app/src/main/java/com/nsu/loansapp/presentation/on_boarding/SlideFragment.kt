package com.nsu.loansapp.presentation.on_boarding

import android.os.Bundle
import android.view.View
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.FragmentSlideBinding

class SlideFragment: Fragment(R.layout.fragment_slide) {

    companion object {
        const val SLIDE_ITEM = "SLIDE_ITEM"

        fun newInstance(slideItem: SlideItem): SlideFragment {
            val fragment = SlideFragment()
            val bundle = bundleOf(SLIDE_ITEM to slideItem)
            fragment.arguments = bundle
            return fragment
        }
    }

    private val binding by viewBinding(FragmentSlideBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            val slideItem = requireArguments().get(SLIDE_ITEM) as SlideItem
            slideImage.setImageDrawable(getDrawable(requireContext(), slideItem.drawable))
            slideText.text = slideItem.text
        }
    }

}