package com.nsu.loansapp.presentation.on_boarding

import android.os.Parcelable
import androidx.annotation.DrawableRes
import kotlinx.parcelize.Parcelize

@Parcelize
data class SlideItem(
    @DrawableRes val drawable: Int,
    val text: String
): Parcelable
