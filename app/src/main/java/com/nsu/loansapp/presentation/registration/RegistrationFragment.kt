package com.nsu.loansapp.presentation.registration

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.FragmentRegistrationBinding
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.closeSoftKeyboard
import com.nsu.loansapp.utils.enums.getMessage
import com.nsu.loansapp.utils.shake
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegistrationFragment: Fragment(R.layout.fragment_registration) {

    private val binding by viewBinding(FragmentRegistrationBinding::bind)
    private val viewModel by viewModels<RegistrationViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initListeners()
        initObservers()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initObservers() {
        viewModel.registerState.observe(viewLifecycleOwner) { status ->
            when(status) {
                is DataStatus.Loading -> renderLoadingState()
                is DataStatus.Success -> renderSuccessState()
                is DataStatus.Error -> renderErrorState(status = status)
            }
        }

        viewModel.fieldsState.observe(viewLifecycleOwner) { isValid ->
            with(binding) {
                if (isValid) {
                    viewModel.register(
                        login = loginField.text.toString().trim(),
                        password = passwordField.text.toString().trim()
                    )
                } else {
                    loginFieldLayout.shake(2, 300).start()
                    passwordFieldLayout.shake(2, 300).start()
                    repeatPasswordFieldLayout.shake(2, 300).start()
                    showMessage(getString(R.string.incorrect_data))
                }
            }

        }
    }

    private fun renderErrorState(status: DataStatus.Error) {
        with(binding) {
            contentContainer.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
        val message = status.errorType.getMessage(requireContext())
        showMessage(message = message)
    }

    private fun renderSuccessState() {
        showMessage(getString(R.string.registration_successful))
        viewModel.navigateBack()
    }

    private fun renderLoadingState() {
        with(binding) {
            progressBar.visibility = View.VISIBLE
            contentContainer.visibility = View.GONE
        }
    }

    private fun initListeners() {
        with(binding) {
            backButton.setOnClickListener {
                viewModel.navigateBack()
            }
            registerButton.setOnClickListener {
                it.closeSoftKeyboard()
                viewModel.validateFields(
                    login = loginField.text?.toString(),
                    password = passwordField.text?.toString(),
                    repeatPassword = repeatPasswordField.text?.toString()
                )
            }
        }

    }

    private fun showMessage(message: String) {
        Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
    }

}