package com.nsu.loansapp.presentation.registration

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.terrakok.cicerone.Router
import com.nsu.loansapp.domain.models.auth.Authorization
import com.nsu.loansapp.domain.models.auth.User
import com.nsu.loansapp.domain.use_cases.auth.RegisterUseCase
import com.nsu.loansapp.utils.DataStatus
import com.nsu.loansapp.utils.events.SingleLiveEvent
import com.nsu.loansapp.utils.getErrorType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegistrationViewModel @Inject constructor(
    private val registerUseCase: RegisterUseCase,
    private val router: Router
): ViewModel() {

    private val _registerState = SingleLiveEvent<DataStatus<User>>()
    val registerState: LiveData<DataStatus<User>> = _registerState

    private val _fieldsState = SingleLiveEvent<Boolean>()
    val fieldsState: LiveData<Boolean> = _fieldsState

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        val errorType = throwable.getErrorType()
        _registerState.value = DataStatus.Error(errorType = errorType)
    }

    fun register(login: String, password: String) {
        viewModelScope.launch(exceptionHandler) {
            _registerState.value = DataStatus.Loading
            val state = registerUseCase(
                Authorization(
                    name = login,
                    password = password
                )
            )
            _registerState.value = state
        }
    }

    fun navigateBack() {
        router.exit()
    }

    fun validateFields(login: String?, password: String?, repeatPassword: String?) {
        _fieldsState.value = !login.isNullOrBlank()
                && !password.isNullOrBlank()
                && !repeatPassword.isNullOrBlank()
                && repeatPassword == password
    }

}