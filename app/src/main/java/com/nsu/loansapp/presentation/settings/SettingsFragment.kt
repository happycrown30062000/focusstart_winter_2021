package com.nsu.loansapp.presentation.settings

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.nsu.loansapp.R
import com.nsu.loansapp.databinding.FragmentSettingsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingsFragment: Fragment(R.layout.fragment_settings) {

    private val viewModel by viewModels<SettingsViewModel>()
    private val binding by viewBinding(FragmentSettingsBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            nightModeSwitch.setOnCheckedChangeListener { _, isChecked ->
                viewModel.setNightMode(isNightMode = isChecked)
            }
            nightModeSwitch.isChecked = viewModel.isInNightMode()

            languageSwitch.setOnCheckedChangeListener { _, isChecked ->
                viewModel.setIsRussian(isRussian = isChecked)
            }
            languageSwitch.isChecked = viewModel.getIsRussian()

            exitButton.setOnClickListener {
                openAlertDialog()
            }
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun openAlertDialog() {
        val dialog = MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.exit_text)
            .setMessage(R.string.exit_sure)
            .setPositiveButton(R.string.ok_text) { dialog, _ ->
                viewModel.logOut()
                dialog.cancel()
            }
            .setNegativeButton(R.string.cancel_text) { dialog, _ ->
                dialog.cancel()
            }
        dialog.show()
    }


}