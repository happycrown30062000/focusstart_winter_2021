package com.nsu.loansapp.presentation.settings

import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.terrakok.cicerone.Router
import com.nsu.loansapp.domain.use_cases.loans.ClearCacheUseCase
import com.nsu.loansapp.domain.use_cases.user_info.DeleteTokenUseCase
import com.nsu.loansapp.domain.use_cases.user_info.GetIsRussianUseCase
import com.nsu.loansapp.domain.use_cases.user_info.SetIsRussianUseCase
import com.nsu.loansapp.domain.use_cases.user_info.SetNightModeUseCase
import com.nsu.loansapp.utils.navigation.Screens
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val setNightModeUseCase: SetNightModeUseCase,
    private val getIsRussianUseCase: GetIsRussianUseCase,
    private val setIsRussianUseCase: SetIsRussianUseCase,
    private val deleteTokenUseCase: DeleteTokenUseCase,
    private val clearCacheUseCase: ClearCacheUseCase,
    private val router: Router
): ViewModel() {

    fun setNightMode(isNightMode: Boolean) {
        setNightModeUseCase(isNightMode = isNightMode)
    }

    fun setIsRussian(isRussian: Boolean) {
        setIsRussianUseCase(isRussian = isRussian)
    }

    fun getIsRussian(): Boolean = getIsRussianUseCase()

    fun isInNightMode(): Boolean {
        return AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES
    }

    fun logOut() {
        viewModelScope.launch {
            deleteTokenUseCase()
            clearCacheUseCase()
            router.newRootScreen(Screens.LoginScreen())
        }
    }

}