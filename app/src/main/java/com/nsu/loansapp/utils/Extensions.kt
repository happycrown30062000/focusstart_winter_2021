package com.nsu.loansapp.utils

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService
import com.nsu.loansapp.utils.enums.ErrorType
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

fun View.closeSoftKeyboard() {
    val imm = getSystemService(this.context, InputMethodManager::class.java)
    imm?.hideSoftInputFromWindow(this.windowToken, 0)
}

fun View.shake(repeatCount: Int, duration: Long): Animator {
    val translationX = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, 0f, -10f, 10f, 0f)
    return ObjectAnimator.ofPropertyValuesHolder(this, translationX).apply {
        this.repeatCount = repeatCount
        this.duration = duration
    }
}

fun Throwable.getErrorType(): ErrorType {
    return when(this) {
        is ConnectException -> ErrorType.NoInternetConnection
        is UnknownHostException -> ErrorType.BadInternetConnection
        is SocketTimeoutException -> ErrorType.SocketTimeOut
        else -> ErrorType.Unknown
    }
}


fun String.toDateString(isRussian: Boolean): String {
    val locale = if (isRussian) Locale("ru") else Locale.US
    val dateTime = OffsetDateTime.parse(this)
    return dateTime.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale))
}
