package com.nsu.loansapp.utils.navigation

import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.nsu.loansapp.presentation.add_loan.AddLoanFragment
import com.nsu.loansapp.presentation.authorization.AuthFragment
import com.nsu.loansapp.presentation.loan_details.LoanDetailsFragment
import com.nsu.loansapp.presentation.main.MainFragment
import com.nsu.loansapp.presentation.on_boarding.OnBoardingFragment
import com.nsu.loansapp.presentation.registration.RegistrationFragment

@Suppress("FunctionName")
object Screens {

    fun MainScreen() = FragmentScreen {
        MainFragment()
    }

    fun LoginScreen() = FragmentScreen {
        AuthFragment()
    }

    fun AddLoanScreen() = FragmentScreen {
        AddLoanFragment()
    }

    fun RegistrationScreen() = FragmentScreen {
        RegistrationFragment()
    }

    fun OnBoardingScreen() = FragmentScreen {
        OnBoardingFragment()
    }

    fun LoanDetailsScreen(id: Int) = FragmentScreen {
        LoanDetailsFragment.newInstance(id = id)
    }

}