package com.nsu.loansapp.data.auth

import com.nsu.loansapp.entities.AuthEntities
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AuthRemoteDataSourceTest {

    private val authApi: AuthorizationApi = mockk {
        coEvery { login(AuthEntities.AUTH_ENTITY) } returns AuthEntities.SUCCESS_LOGIN_RESPONSE
        coEvery { register(AuthEntities.AUTH_ENTITY) } returns AuthEntities.SUCCESS_REGISTER_RESPONSE
    }
    private val authDataSource = AuthRemoteDataSource(authorizationApi = authApi)


    @Test
    fun `login EXPEXT response with token`() = runBlocking {
        val response = authDataSource.login(AuthEntities.AUTH_ENTITY)
        assertEquals(AuthEntities.SUCCESS_LOGIN_RESPONSE, response)
    }

    @Test
    fun `register EXPECT response with user`() = runBlocking {
        val response = authDataSource.register(AuthEntities.AUTH_ENTITY)
        assertEquals(AuthEntities.SUCCESS_REGISTER_RESPONSE, response)
    }




}