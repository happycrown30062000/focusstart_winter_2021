package com.nsu.loansapp.data.auth

import com.nsu.loansapp.entities.AuthEntities
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AuthRepositoryImplTest {

    private val authDataSource: AuthDataSource = mockk {
        coEvery { login(AuthEntities.AUTH_ENTITY) } returns AuthEntities.SUCCESS_LOGIN_RESPONSE
        coEvery { register(AuthEntities.AUTH_ENTITY) } returns AuthEntities.SUCCESS_REGISTER_RESPONSE
    }
    private val authRepositoryImpl = AuthRepositoryImpl(authDataSource)

    @Test
    fun `login EXPECT Success DataStatus with token`() = runBlocking {
        val status = authRepositoryImpl.login(AuthEntities.AUTH_ENTITY)
        assertEquals(AuthEntities.SUCCESS_LOGIN_STATUS, status)
    }

    @Test
    fun `register EXPECT Success DataStatus with user`() = runBlocking {
        val status = authRepositoryImpl.register(AuthEntities.AUTH_ENTITY)
        assertEquals(AuthEntities.SUCCESS_REGISTER_STATUS, status)
    }

}