package com.nsu.loansapp.data.loans

import com.nsu.loansapp.data.loans.local.LoansCacheDataSourceImpl
import com.nsu.loansapp.data.loans.local.LoansDao
import com.nsu.loansapp.entities.LoanEntities
import io.mockk.coEvery
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoansCacheDataSourceImplTest {

    private val loansDao: LoansDao = mockk {
        coEvery { getAllLoans() } returns LoanEntities.LOANS_ENTITIES_LIST
        coJustRun { insertAllLoans(LoanEntities.LOANS_ENTITIES_LIST) }
        coJustRun { deleteAll() }
    }

    private val loansCacheDataSourceImpl = LoansCacheDataSourceImpl(loansDao)

    @Test
    fun `get all loans EXPECT correct list`() = runBlocking {
        val list = loansCacheDataSourceImpl.getAllLoans()
        assertEquals(LoanEntities.LOANS_ENTITIES_LIST, list)
    }

    @Test
    fun `insert all loans EXPECT insert in dao`() = runBlocking {
        loansCacheDataSourceImpl.insertAllLoans(LoanEntities.LOANS_ENTITIES_LIST)
        coVerify { loansDao.insertAllLoans(LoanEntities.LOANS_ENTITIES_LIST) }
    }

    @Test
    fun `delete all loans EXPECT delete in dao`() = runBlocking {
        loansCacheDataSourceImpl.deleteAll()
        coVerify { loansDao.deleteAll() }
    }

}