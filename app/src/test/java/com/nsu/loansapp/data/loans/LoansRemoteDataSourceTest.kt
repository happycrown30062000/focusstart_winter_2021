package com.nsu.loansapp.data.loans

import com.nsu.loansapp.data.loans.remote.LoansApi
import com.nsu.loansapp.data.loans.remote.LoansRemoteDataSource
import com.nsu.loansapp.entities.LoanEntities
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoansRemoteDataSourceTest {

    private val loansApi: LoansApi = mockk {
        coEvery { createLoan(LoanEntities.LOAN_REQUEST) } returns LoanEntities.SUCCESS_CREATE_LOAN_RESPONSE
        coEvery { getLoan(LoanEntities.LOAN_ID) } returns LoanEntities.SUCCESS_GET_LOAN_RESPONSE
        coEvery { getAllLoans() } returns LoanEntities.SUCCESS_GET_ALL_LOANS_RESPONSE
        coEvery { getLoanConditions() } returns LoanEntities.SUCCESS_GET_LOAN_CONDITIONS_RESPONSE
    }
    private val loansDataSource = LoansRemoteDataSource(loansApi)

    @Test
    fun `create loan EXPECT successfully created loan`() = runBlocking {
        val response = loansDataSource.createLoan(LoanEntities.LOAN_REQUEST)
        assertEquals(LoanEntities.SUCCESS_CREATE_LOAN_RESPONSE, response)
    }

    @Test
    fun `get loan EXPECT response with loan`() = runBlocking {
        val response = loansDataSource.getLoan(LoanEntities.LOAN_ID)
        assertEquals(LoanEntities.SUCCESS_GET_LOAN_RESPONSE, response)
    }

    @Test
    fun `get all loans EXPECT response with loans list`() = runBlocking {
        val response = loansDataSource.getAllLoans()
        assertEquals(LoanEntities.SUCCESS_GET_ALL_LOANS_RESPONSE, response)
    }

    @Test
    fun `get loan conditions EXPECT response with loan conditions`() = runBlocking {
        val response = loansDataSource.getLoanConditions()
        assertEquals(LoanEntities.SUCCESS_GET_LOAN_CONDITIONS_RESPONSE, response)
    }

}