package com.nsu.loansapp.data.loans

import com.nsu.loansapp.data.loans.local.LoansCacheDataSource
import com.nsu.loansapp.entities.LoanEntities
import io.mockk.coEvery
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.mockk
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoansRepositoryImplTest {

    private val loansDataSource: LoansDataSource = mockk {
        coEvery { createLoan(LoanEntities.LOAN_REQUEST) } returns LoanEntities.SUCCESS_CREATE_LOAN_RESPONSE
        coEvery { getLoan(LoanEntities.LOAN_ID) } returns LoanEntities.SUCCESS_GET_LOAN_RESPONSE
        coEvery { getAllLoans() } returns LoanEntities.SUCCESS_GET_ALL_LOANS_RESPONSE
        coEvery { getLoanConditions() } returns LoanEntities.SUCCESS_GET_LOAN_CONDITIONS_RESPONSE
    }

    private val loansCacheDataSource: LoansCacheDataSource = mockk {
        coEvery { getAllLoans() } returns LoanEntities.LOANS_ENTITIES_LIST
        coJustRun { deleteAll() }
        coJustRun { insertAllLoans(LoanEntities.LOANS_ENTITIES_LIST) }
    }

    private val loansRepositoryImpl = LoansRepositoryImpl(
        loansDataSource = loansDataSource,
        loansCacheDataSource = loansCacheDataSource
    )

    @Test
    fun `save loans EXPECT loans save in data source`() = runBlocking {
        loansRepositoryImpl.saveAllLoans(LoanEntities.LOANS_LIST)
        coVerify { loansCacheDataSource.insertAllLoans(LoanEntities.LOANS_ENTITIES_LIST) }
    }

    @Test
    fun `clear loans EXPECT loans delete in data source`() = runBlocking {
        loansRepositoryImpl.clearCache()
        coVerify { loansCacheDataSource.deleteAll() }
    }

    @Test
    fun `create loan EXPECT success loan status`() = runBlocking {
        val status = loansRepositoryImpl.createLoan(LoanEntities.LOAN_REQUEST)
        TestCase.assertEquals(LoanEntities.SUCCESS_CREATE_LOAN_STATUS, status)
    }

    @Test
    fun `get loan EXPECT success loan status`() = runBlocking {
        val status = loansRepositoryImpl.getLoan(LoanEntities.LOAN_ID)
        TestCase.assertEquals(LoanEntities.SUCCESS_GET_LOAN_STATUS, status)
    }

    @Test
    fun `get all loans from remote EXPECT success loans list status`() = runBlocking {
        val status = loansRepositoryImpl.getAllLoans(isFromCache = false)
        TestCase.assertEquals(LoanEntities.SUCCESS_GET_ALL_LOANS_STATUS, status)
    }

    @Test
    fun `get all loans from cache EXPECT success loans list status`() = runBlocking {
        val status = loansRepositoryImpl.getAllLoans(isFromCache = true)
        TestCase.assertEquals(LoanEntities.SUCCESS_GET_ALL_LOANS_STATUS, status)
    }

    @Test
    fun `get loan conditions EXPECT success loan conditions status`() = runBlocking {
        val status = loansRepositoryImpl.getLoanConditions()
        TestCase.assertEquals(LoanEntities.SUCCESS_GET_LOAN_CONDITIONS_STATUS, status)
    }

}