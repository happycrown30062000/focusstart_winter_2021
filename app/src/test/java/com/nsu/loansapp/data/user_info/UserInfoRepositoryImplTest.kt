package com.nsu.loansapp.data.user_info

import com.nsu.loansapp.entities.UserInfoEntities
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UserInfoRepositoryImplTest {

    private val userInfoDataSource: UserInfoDataSource = mockk {
        justRun { saveToken(UserInfoEntities.TOKEN) }
        every { getToken() } returns UserInfoEntities.TOKEN

        justRun { setFirstEnter() }
        every { isFirstEnter() } returns UserInfoEntities.IS_FIRST_ENTER

        every { isNightMode() } returns UserInfoEntities.IS_IN_NIGHT_MODE
        justRun { setNightMode(UserInfoEntities.IS_IN_NIGHT_MODE) }

        every { getIsRussian() } returns UserInfoEntities.IS_RUSSIAN
        justRun { setIsRussian(UserInfoEntities.IS_RUSSIAN) }

        justRun { deleteToken() }
    }

    private val userInfoRepository = UserInfoRepositoryImpl(userInfoDataSource)

    @Test
    fun `save token EXPECT token saves in data source`() {
        userInfoRepository.saveToken(UserInfoEntities.TOKEN)
        verify { userInfoDataSource.saveToken(UserInfoEntities.TOKEN) }
    }

    @Test
    fun `get token EXPECT correct token`() {
        val token = userInfoRepository.getToken()
        assertEquals(UserInfoEntities.TOKEN, token)
    }

    @Test
    fun `delete token EXPECT token delete in data source`() {
        userInfoRepository.deleteToken()
        verify { userInfoDataSource.deleteToken() }
    }

    @Test
    fun `set first enter EXPECT first enter set in data source`() {
        userInfoRepository.setFirstEnter()
        verify { userInfoDataSource.setFirstEnter() }
    }

    @Test
    fun `get first enter EXPECT correct first enter value`() {
        val isFirstEnter = userInfoRepository.isFirstEnter()
        assertEquals(UserInfoEntities.IS_FIRST_ENTER, isFirstEnter)
    }

    @Test
    fun `set night mode EXPECT night mode set in data source`() {
        userInfoRepository.setNightMode(UserInfoEntities.IS_IN_NIGHT_MODE)
        verify { userInfoDataSource.setNightMode(UserInfoEntities.IS_IN_NIGHT_MODE) }
    }

    @Test
    fun `get is in night mode EXPECT correct is night mode value`() {
        val isNightMode = userInfoRepository.isNightMode()
        assertEquals(UserInfoEntities.IS_IN_NIGHT_MODE, isNightMode)
    }

    @Test
    fun `set use russian EXPECT use russian set in data source`() {
        userInfoRepository.setIsRussian(UserInfoEntities.IS_RUSSIAN)
        verify { userInfoDataSource.setIsRussian(UserInfoEntities.IS_RUSSIAN) }
    }

    @Test
    fun `get is russian EXPECT correct is russian value`() {
        val isRussian = userInfoRepository.getIsRussian()
        assertEquals(UserInfoEntities.IS_RUSSIAN, isRussian)
    }

}