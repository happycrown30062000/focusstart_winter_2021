package com.nsu.loansapp.domain.use_cases.auth

import com.nsu.loansapp.domain.repositories.AuthRepository
import com.nsu.loansapp.entities.AuthEntities
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoginUseCaseTest {

    private val authRepository: AuthRepository = mockk {
        coEvery { login(AuthEntities.AUTH_ENTITY) } returns AuthEntities.SUCCESS_LOGIN_STATUS
    }

    private val loginUseCase = LoginUseCase(authRepository)

    @Test
    fun `invoke login use case EXPECT success login status`() = runBlocking {
        val status = loginUseCase(AuthEntities.AUTH_ENTITY)
        assertEquals(AuthEntities.SUCCESS_LOGIN_STATUS, status)
    }

}