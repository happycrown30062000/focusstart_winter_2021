package com.nsu.loansapp.domain.use_cases.auth

import com.nsu.loansapp.domain.repositories.AuthRepository
import com.nsu.loansapp.entities.AuthEntities
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RegisterUseCaseTest {

    private val authRepository: AuthRepository = mockk {
        coEvery { register(AuthEntities.AUTH_ENTITY) } returns AuthEntities.SUCCESS_REGISTER_STATUS
    }

    private val registerUseCase = RegisterUseCase(authRepository)

    @Test
    fun `invoke register use case EXPECT success login status`() = runBlocking {
        val status = registerUseCase(AuthEntities.AUTH_ENTITY)
        TestCase.assertEquals(AuthEntities.SUCCESS_REGISTER_STATUS, status)
    }
}