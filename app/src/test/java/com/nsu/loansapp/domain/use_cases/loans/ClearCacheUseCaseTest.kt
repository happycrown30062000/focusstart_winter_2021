package com.nsu.loansapp.domain.use_cases.loans

import com.nsu.loansapp.domain.repositories.LoansRepository
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ClearCacheUseCaseTest {

    private val loansRepository: LoansRepository = mockk {
        coJustRun { clearCache() }
    }

    private val clearCacheUseCase = ClearCacheUseCase(loansRepository)

    @Test
    fun `invoke clear cache use case EXPECT clear cache in repository`() = runBlocking {
        clearCacheUseCase()
        coVerify { loansRepository.clearCache() }
    }
}