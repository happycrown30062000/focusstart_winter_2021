package com.nsu.loansapp.domain.use_cases.loans

import com.nsu.loansapp.domain.repositories.LoansRepository
import com.nsu.loansapp.entities.LoanEntities
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CreateLoanUseCaseTest {

    private val loansRepository: LoansRepository = mockk {
        coEvery { createLoan(LoanEntities.LOAN_REQUEST) } returns LoanEntities.SUCCESS_CREATE_LOAN_STATUS
    }

    private val createLoanUseCase = CreateLoanUseCase(loansRepository)

    @Test
    fun `invoke create loan use case EXPECT success create status`() = runBlocking {
        val status = createLoanUseCase(LoanEntities.LOAN_REQUEST)
        assertEquals(LoanEntities.SUCCESS_CREATE_LOAN_STATUS, status)
    }

}