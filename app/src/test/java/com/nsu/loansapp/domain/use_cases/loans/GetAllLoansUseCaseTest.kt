package com.nsu.loansapp.domain.use_cases.loans

import com.nsu.loansapp.domain.repositories.LoansRepository
import com.nsu.loansapp.entities.LoanEntities
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetAllLoansUseCaseTest {

    private val loansRepository: LoansRepository = mockk {
        coEvery { getAllLoans(isFromCache = any()) } returns LoanEntities.SUCCESS_GET_ALL_LOANS_STATUS
    }

    private val getAllLoansUseCase = GetAllLoansUseCase(loansRepository)

    @Test
    fun `invoke get all loans use case from cache EXPECT success list status`() = runBlocking {
        val status = getAllLoansUseCase(isFromCache = true)
        TestCase.assertEquals(LoanEntities.SUCCESS_GET_ALL_LOANS_STATUS, status)
    }

    @Test
    fun `invoke get all loans use case from remote EXPECT success list status`() = runBlocking {
        val status = getAllLoansUseCase(isFromCache = false)
        TestCase.assertEquals(LoanEntities.SUCCESS_GET_ALL_LOANS_STATUS, status)
    }
}