package com.nsu.loansapp.domain.use_cases.loans

import com.nsu.loansapp.domain.repositories.LoansRepository
import com.nsu.loansapp.entities.LoanEntities
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetLoanConditionsUseCaseTest {

    private val loansRepository: LoansRepository = mockk {
        coEvery { getLoanConditions() } returns LoanEntities.SUCCESS_GET_LOAN_CONDITIONS_STATUS
    }

    private val getLoanConditions = GetLoanConditionsUseCase(loansRepository)

    @Test
    fun `invoke get loan conditions use case EXPECT success conditions status`() = runBlocking {
        val status = getLoanConditions()
        TestCase.assertEquals(LoanEntities.SUCCESS_GET_LOAN_CONDITIONS_STATUS, status)
    }
}