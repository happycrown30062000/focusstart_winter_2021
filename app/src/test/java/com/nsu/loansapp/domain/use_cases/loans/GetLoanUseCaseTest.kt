package com.nsu.loansapp.domain.use_cases.loans

import com.nsu.loansapp.domain.repositories.LoansRepository
import com.nsu.loansapp.entities.LoanEntities
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetLoanUseCaseTest {

    private val loansRepository: LoansRepository = mockk {
        coEvery { getLoan(LoanEntities.LOAN_ID) } returns LoanEntities.SUCCESS_GET_LOAN_STATUS
    }

    private val getLoanUseCase = GetLoanUseCase(loansRepository)

    @Test
    fun `invoke get loan use case EXPECT success loan status`() = runBlocking {
        val status = getLoanUseCase(LoanEntities.LOAN_ID)
        TestCase.assertEquals(LoanEntities.SUCCESS_GET_LOAN_STATUS, status)
    }
}