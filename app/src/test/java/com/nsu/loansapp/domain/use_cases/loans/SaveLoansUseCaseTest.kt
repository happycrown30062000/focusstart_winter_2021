package com.nsu.loansapp.domain.use_cases.loans

import com.nsu.loansapp.domain.repositories.LoansRepository
import com.nsu.loansapp.entities.LoanEntities
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SaveLoansUseCaseTest {

    private val loansRepository: LoansRepository = mockk {
        coJustRun { saveAllLoans(LoanEntities.LOANS_LIST) }
    }

    private val saveLoansUseCase = SaveLoansUseCase(loansRepository)

    @Test
    fun `invoke save loans use case ECPECT save loans in repository`() = runBlocking {
        saveLoansUseCase(LoanEntities.LOANS_LIST)
        coVerify { loansRepository.saveAllLoans(LoanEntities.LOANS_LIST) }
    }
}