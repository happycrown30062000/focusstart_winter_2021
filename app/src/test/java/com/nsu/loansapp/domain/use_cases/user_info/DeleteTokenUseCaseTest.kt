package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DeleteTokenUseCaseTest {

    private val userInfoRepository: UserInfoRepository = mockk {
        justRun { deleteToken() }
    }

    private val deleteTokenUseCase = DeleteTokenUseCase(userInfoRepository)

    @Test
    fun `invoke EXPECT delete token in repository`() {
        deleteTokenUseCase()
        verify { userInfoRepository.deleteToken() }
    }
}