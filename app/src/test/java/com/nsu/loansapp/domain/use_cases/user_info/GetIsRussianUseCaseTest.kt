package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import com.nsu.loansapp.entities.UserInfoEntities
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.*

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetIsRussianUseCaseTest {

    private val userInfoRepository: UserInfoRepository = mockk {
        every { getIsRussian() } returns UserInfoEntities.IS_RUSSIAN
    }

    private val getIsRussianUseCase = GetIsRussianUseCase(userInfoRepository)

    @Test
    fun `invoke get is russian use case EXPECT correct is russian value`() {
        val isRussian = getIsRussianUseCase()
        assertEquals(UserInfoEntities.IS_RUSSIAN, isRussian)
    }
}