package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import com.nsu.loansapp.entities.UserInfoEntities
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class IsFirstEnterUseCaseTest {

    private val userInfoRepository: UserInfoRepository = mockk {
        every { isFirstEnter() } returns UserInfoEntities.IS_FIRST_ENTER
    }

    private val isFirstEnterUseCase = IsFirstEnterUseCase(userInfoRepository)

    @Test
    fun `invoke get is russian use case EXPECT correct is russian value`() {
        val isFirstEnter = isFirstEnterUseCase()
        assertEquals(UserInfoEntities.IS_FIRST_ENTER, isFirstEnter)
    }

}