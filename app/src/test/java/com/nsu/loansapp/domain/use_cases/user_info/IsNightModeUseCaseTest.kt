package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import com.nsu.loansapp.entities.UserInfoEntities
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class IsNightModeUseCaseTest {

    private val userInfoRepository: UserInfoRepository = mockk {
        every { isNightMode() } returns UserInfoEntities.IS_IN_NIGHT_MODE
    }

    private val isNightModeUseCse = IsNightModeUseCase(userInfoRepository)

    @Test
    fun `invoke get is russian use case EXPECT correct is russian value`() {
        val isNightMode = isNightModeUseCse()
        assertEquals(UserInfoEntities.IS_IN_NIGHT_MODE, isNightMode)
    }

}