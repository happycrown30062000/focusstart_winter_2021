package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import com.nsu.loansapp.entities.UserInfoEntities
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SaveTokenUseCaseTest {

    private val userInfoRepository: UserInfoRepository = mockk {
        justRun { saveToken(UserInfoEntities.TOKEN) }
    }

    private val saveTokenUseCase = SaveTokenUseCase(userInfoRepository)

    @Test
    fun `invoke save token use case EXPECT save token in repository`() {
        saveTokenUseCase(UserInfoEntities.TOKEN)
        verify { userInfoRepository.saveToken(UserInfoEntities.TOKEN) }
    }

}