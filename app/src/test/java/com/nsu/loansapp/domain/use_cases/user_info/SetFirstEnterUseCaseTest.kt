package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SetFirstEnterUseCaseTest {

    private val userInfoRepository: UserInfoRepository = mockk {
        justRun { setFirstEnter() }
    }

    private val setFirstEnterUseCase = SetFirstEnterUseCase(userInfoRepository)

    @Test
    fun `invoke set first enter use case EXPECT save first enter in repository`() {
        setFirstEnterUseCase()
        verify { userInfoRepository.setFirstEnter() }
    }

}