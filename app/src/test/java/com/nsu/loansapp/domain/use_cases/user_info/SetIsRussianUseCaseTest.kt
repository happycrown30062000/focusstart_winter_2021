package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import com.nsu.loansapp.entities.UserInfoEntities
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SetIsRussianUseCaseTest {

    private val userInfoRepository: UserInfoRepository = mockk {
        justRun { setIsRussian(UserInfoEntities.IS_RUSSIAN) }
    }

    private val setIsRussianUseCase = SetIsRussianUseCase(userInfoRepository)

    @Test
    fun `invoke get is russian use case EXPECT save in repository`() {
        setIsRussianUseCase(UserInfoEntities.IS_RUSSIAN)
        verify { userInfoRepository.setIsRussian(UserInfoEntities.IS_RUSSIAN) }
    }

}