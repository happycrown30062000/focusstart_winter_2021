package com.nsu.loansapp.domain.use_cases.user_info

import com.nsu.loansapp.domain.repositories.UserInfoRepository
import com.nsu.loansapp.entities.UserInfoEntities
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SetNightModeUseCaseTest {

    private val userInfoRepository: UserInfoRepository = mockk {
        justRun { setNightMode(UserInfoEntities.IS_IN_NIGHT_MODE) }
    }

    private val setNightModeUseCase = SetNightModeUseCase(userInfoRepository)

    @Test
    fun `invoke set night mode use case EXPECT night mode set in repository`() {
        setNightModeUseCase(UserInfoEntities.IS_IN_NIGHT_MODE)
        verify { userInfoRepository.setNightMode(UserInfoEntities.IS_IN_NIGHT_MODE) }
    }

}