package com.nsu.loansapp.entities

import com.nsu.loansapp.domain.models.auth.Authorization
import com.nsu.loansapp.domain.models.auth.User
import com.nsu.loansapp.utils.DataStatus
import retrofit2.Response

object AuthEntities {

    private const val TOKEN = "token"

    val AUTH_ENTITY = Authorization(
        name = "name",
        password = "password"
    )

    private val USER_ENTITY = User(
        name = "name",
        role = "role"
    )

    val SUCCESS_LOGIN_RESPONSE: Response<String> = Response.success(TOKEN)
    val SUCCESS_REGISTER_RESPONSE: Response<User> = Response.success(USER_ENTITY)

    val SUCCESS_LOGIN_STATUS: DataStatus<String> = DataStatus.Success(TOKEN)
    val SUCCESS_REGISTER_STATUS: DataStatus<User> = DataStatus.Success(USER_ENTITY)

}