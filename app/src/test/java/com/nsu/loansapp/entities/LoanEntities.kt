package com.nsu.loansapp.entities

import com.nsu.loansapp.data.loans.local.LoanEntity
import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.domain.models.loans.LoanConditions
import com.nsu.loansapp.domain.models.loans.LoanRequest
import com.nsu.loansapp.utils.DataStatus
import retrofit2.Response

object LoanEntities {

    const val LOAN_ID = 10

    val LOAN_REQUEST = LoanRequest(
        amount = 10,
        firstName = "firstName",
        lastName = "lastName",
        percent = 10.0,
        period = 300,
        phoneNumber = "88005553535"
    )

    private val LOAN = Loan(
        amount = 10,
        date = "2020-20-20",
        firstName = "firstName",
        lastName = "lastName",
        percent = 10.0,
        period = 300,
        phoneNumber = "88005553535",
        id = 142124,
        state = "APPROVED"
    )

    private val LOAN_ENTITY = LoanEntity(
        amount = 10,
        date = "2020-20-20",
        firstName = "firstName",
        lastName = "lastName",
        percent = 10.0,
        period = 300,
        phoneNumber = "88005553535",
        id = 142124,
        state = "APPROVED"
    )

    private val LOAN_CONDITIONS = LoanConditions(
        maxAmount = 30000,
        percent = 10.0,
        period = 300
    )

    val LOANS_LIST = listOf(
        LOAN,
        LOAN
    )

    val LOANS_ENTITIES_LIST = listOf(
        LOAN_ENTITY,
        LOAN_ENTITY
    )

    val SUCCESS_CREATE_LOAN_RESPONSE: Response<Loan> = Response.success(LOAN)
    val SUCCESS_GET_LOAN_RESPONSE: Response<Loan> = Response.success(LOAN)
    val SUCCESS_GET_ALL_LOANS_RESPONSE: Response<List<Loan>> = Response.success(LOANS_LIST)
    val SUCCESS_GET_LOAN_CONDITIONS_RESPONSE: Response<LoanConditions> = Response.success(LOAN_CONDITIONS)

    val SUCCESS_CREATE_LOAN_STATUS: DataStatus<Loan> = DataStatus.Success(LOAN)
    val SUCCESS_GET_LOAN_STATUS: DataStatus<Loan> = DataStatus.Success(LOAN)
    val SUCCESS_GET_ALL_LOANS_STATUS: DataStatus<List<Loan>> = DataStatus.Success(LOANS_LIST)
    val SUCCESS_GET_LOAN_CONDITIONS_STATUS: DataStatus<LoanConditions> = DataStatus.Success(LOAN_CONDITIONS)

}