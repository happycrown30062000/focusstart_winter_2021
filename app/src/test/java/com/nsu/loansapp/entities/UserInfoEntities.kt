package com.nsu.loansapp.entities

object UserInfoEntities {

    const val TOKEN = "token"
    const val IS_IN_NIGHT_MODE = false
    const val IS_FIRST_ENTER = true
    const val IS_RUSSIAN = false

}