package com.nsu.loansapp.presentation.add_loan

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nsu.loansapp.domain.models.loans.Loan
import com.nsu.loansapp.domain.models.loans.LoanConditions
import com.nsu.loansapp.domain.use_cases.loans.CreateLoanUseCase
import com.nsu.loansapp.domain.use_cases.loans.GetLoanConditionsUseCase
import com.nsu.loansapp.entities.LoanEntities
import com.nsu.loansapp.presentation.TestCoroutineRule
import com.nsu.loansapp.utils.DataStatus
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AddLoanViewModelTest {

    @get:Rule
    val instantRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = TestCoroutineRule()

    private val getLoanConditionsUseCase: GetLoanConditionsUseCase = mockk {
        coEvery { this@mockk.invoke() } returns LoanEntities.SUCCESS_GET_LOAN_CONDITIONS_STATUS
    }

    private val createLoanUseCase: CreateLoanUseCase = mockk {
        coEvery { this@mockk.invoke(LoanEntities.LOAN_REQUEST) } returns LoanEntities.SUCCESS_CREATE_LOAN_STATUS
    }

    private val conditionsObserver: Observer<DataStatus<LoanConditions>> = mockk(relaxed = true)
    private val loanCreateObserver: Observer<DataStatus<Loan>> = mockk(relaxed = true)

    @Test
    fun `get loan conditions EXPECT success status`() {
        coroutineRule.runBlockingTest {
            val viewModel = AddLoanViewModel(
                getLoanConditionsUseCase = getLoanConditionsUseCase,
                createLoanUseCase = createLoanUseCase
            )
            viewModel.conditionState.observeForever(conditionsObserver)

            viewModel.loadConditions()

            verifyOrder {
                conditionsObserver.onChanged(DataStatus.Loading)
                conditionsObserver.onChanged(LoanEntities.SUCCESS_GET_LOAN_CONDITIONS_STATUS)
            }
        }
    }

    @Test
    fun `get loan conditions EXPECT use case invoke`() {
        coroutineRule.runBlockingTest {
            val viewModel = AddLoanViewModel(
                getLoanConditionsUseCase = getLoanConditionsUseCase,
                createLoanUseCase = createLoanUseCase
            )
            viewModel.conditionState.observeForever(conditionsObserver)

            viewModel.loadConditions()

            coVerify {
                getLoanConditionsUseCase()
            }
        }
    }

    @Test
    fun `create loan EXPECT loan successfully created`() {
        coroutineRule.runBlockingTest {
            val viewModel = AddLoanViewModel(
                getLoanConditionsUseCase = getLoanConditionsUseCase,
                createLoanUseCase = createLoanUseCase
            )
            viewModel.createState.observeForever(loanCreateObserver)

            viewModel.loadConditions()
            viewModel.createLoan(
                firstName = LoanEntities.LOAN_REQUEST.firstName,
                lastName = LoanEntities.LOAN_REQUEST.lastName,
                phoneNumber = LoanEntities.LOAN_REQUEST.phoneNumber,
                amount = LoanEntities.LOAN_REQUEST.amount
            )

            verifyOrder {
                loanCreateObserver.onChanged(DataStatus.Loading)
                loanCreateObserver.onChanged(LoanEntities.SUCCESS_CREATE_LOAN_STATUS)
            }
        }
    }

}