package com.nsu.loansapp.presentation.authorization

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.github.terrakok.cicerone.Router
import com.nsu.loansapp.domain.use_cases.auth.LoginUseCase
import com.nsu.loansapp.domain.use_cases.user_info.GetTokenUseCase
import com.nsu.loansapp.domain.use_cases.user_info.IsFirstEnterUseCase
import com.nsu.loansapp.domain.use_cases.user_info.SaveTokenUseCase
import com.nsu.loansapp.domain.use_cases.user_info.SetFirstEnterUseCase
import com.nsu.loansapp.entities.AuthEntities
import com.nsu.loansapp.entities.UserInfoEntities
import com.nsu.loansapp.presentation.TestCoroutineRule
import com.nsu.loansapp.utils.DataStatus
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AuthViewModelTest {

    @get:Rule
    val instantRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = TestCoroutineRule()

    private val loginUseCase: LoginUseCase = mockk {
        coEvery { this@mockk.invoke(AuthEntities.AUTH_ENTITY) } returns AuthEntities.SUCCESS_LOGIN_STATUS
    }
    private val saveTokenUseCase: SaveTokenUseCase = mockk {
        justRun { this@mockk.invoke(UserInfoEntities.TOKEN) }
    }
    private val isFirstEnterUseCase: IsFirstEnterUseCase = mockk()
    private val setFirstEnterUseCase: SetFirstEnterUseCase = mockk()
    private val getTokenUseCase: GetTokenUseCase = mockk()
    private val router: Router = mockk()

    private val loginObserver: Observer<DataStatus<String>> = mockk(relaxed = true)

    @Test
    fun `login EXPECT success login status`() {
        coroutineRule.runBlockingTest {
            val viewModel = AuthViewModel(
                loginUseCase = loginUseCase,
                saveTokenUseCase = saveTokenUseCase,
                isFirstEnterUseCase = isFirstEnterUseCase,
                setFirstEnterUseCase = setFirstEnterUseCase,
                getTokenUseCase = getTokenUseCase,
                router = router
            )
            viewModel.loginState.observeForever(loginObserver)

            viewModel.login(
                login = AuthEntities.AUTH_ENTITY.name,
                password = AuthEntities.AUTH_ENTITY.password
            )

            verifyOrder {
                loginObserver.onChanged(DataStatus.Loading)
                loginObserver.onChanged(AuthEntities.SUCCESS_LOGIN_STATUS)
            }
        }
    }

    @Test
    fun `save token EXPECT token saves`() {
        val viewModel = AuthViewModel(
            loginUseCase = loginUseCase,
            saveTokenUseCase = saveTokenUseCase,
            isFirstEnterUseCase = isFirstEnterUseCase,
            setFirstEnterUseCase = setFirstEnterUseCase,
            getTokenUseCase = getTokenUseCase,
            router = router
        )

        viewModel.saveToken(UserInfoEntities.TOKEN)

        verify {
            saveTokenUseCase(UserInfoEntities.TOKEN)
        }
    }

}