package com.nsu.loansapp.presentation.authorization

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.github.terrakok.cicerone.Router
import com.nsu.loansapp.domain.use_cases.auth.LoginUseCase
import com.nsu.loansapp.domain.use_cases.user_info.GetTokenUseCase
import com.nsu.loansapp.domain.use_cases.user_info.IsFirstEnterUseCase
import com.nsu.loansapp.domain.use_cases.user_info.SaveTokenUseCase
import com.nsu.loansapp.domain.use_cases.user_info.SetFirstEnterUseCase
import io.mockk.mockk
import io.mockk.verify
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class AuthViewModelValidationParametrizedTest(
    private val authData: AuthData
) {

    @get:Rule
    val instantRule = InstantTaskExecutorRule()

    data class AuthData(
        val password: String?,
        val login: String?,
        val result: Boolean
    )

    private val loginUseCase: LoginUseCase = mockk()
    private val saveTokenUseCase: SaveTokenUseCase = mockk()
    private val isFirstEnterUseCase: IsFirstEnterUseCase = mockk()
    private val setFirstEnterUseCase: SetFirstEnterUseCase = mockk()
    private val getTokenUseCase: GetTokenUseCase = mockk()
    private val router: Router = mockk()

    private val fieldsObserver: Observer<Boolean> = mockk(relaxed = true)

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun getData() = listOf(
            AuthData(login = null, password = null, result = false),
            AuthData(login = "login", password = null, result = false),
            AuthData(login = null, password = "password", result = false),
            AuthData(login = "login", password = "password", result = true),
        )
    }

    @Test
    fun `validate login data EXPECT correct result`() {
        val viewModel = AuthViewModel(
            loginUseCase = loginUseCase,
            saveTokenUseCase = saveTokenUseCase,
            isFirstEnterUseCase = isFirstEnterUseCase,
            setFirstEnterUseCase = setFirstEnterUseCase,
            getTokenUseCase = getTokenUseCase,
            router = router
        )
        viewModel.fieldsState.observeForever(fieldsObserver)

        viewModel.validateFields(
            login = authData.login,
            password = authData.password
        )

        verify {
            fieldsObserver.onChanged(authData.result)
        }
    }
}