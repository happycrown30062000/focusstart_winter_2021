package com.nsu.loansapp.presentation.registration

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.github.terrakok.cicerone.Router
import com.nsu.loansapp.domain.models.auth.User
import com.nsu.loansapp.domain.use_cases.auth.RegisterUseCase
import com.nsu.loansapp.entities.AuthEntities
import com.nsu.loansapp.presentation.TestCoroutineRule
import com.nsu.loansapp.utils.DataStatus
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verifyOrder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class RegistrationViewModelTest {

    @get:Rule
    val instantRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = TestCoroutineRule()

    private val registerUseCase: RegisterUseCase = mockk {
        coEvery { this@mockk.invoke(AuthEntities.AUTH_ENTITY) } returns AuthEntities.SUCCESS_REGISTER_STATUS
    }

    private val router: Router = mockk()

    private val registerObserver: Observer<DataStatus<User>> = mockk(relaxed = true)

    @Test
    fun `register EXPECT success register state`() {
        coroutineRule.runBlockingTest {
            val viewModel = RegistrationViewModel(
                registerUseCase = registerUseCase,
                router = router
            )
            viewModel.registerState.observeForever(registerObserver)

            viewModel.register(
                login = AuthEntities.AUTH_ENTITY.name,
                password = AuthEntities.AUTH_ENTITY.password
            )

            verifyOrder {
                registerObserver.onChanged(DataStatus.Loading)
                registerObserver.onChanged(AuthEntities.SUCCESS_REGISTER_STATUS)
            }
        }
    }

}