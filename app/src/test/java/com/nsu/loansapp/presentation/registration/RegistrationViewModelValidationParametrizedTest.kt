package com.nsu.loansapp.presentation.registration

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.github.terrakok.cicerone.Router
import com.nsu.loansapp.domain.use_cases.auth.RegisterUseCase
import com.nsu.loansapp.entities.AuthEntities
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verify
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class RegistrationViewModelValidationParametrizedTest(
    private val registerData: RegisterData
) {

    @get:Rule
    val instantRule = InstantTaskExecutorRule()

    data class RegisterData(
        val password: String?,
        val login: String?,
        val repeatPassword: String?,
        val result: Boolean
    )

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun getData() = listOf(
            RegisterData(login = null, password = null, repeatPassword = null, result = false),
            RegisterData(login = "login", password = null, repeatPassword = null, result = false),
            RegisterData(login = "login", password = "password", repeatPassword = null, result = false),
            RegisterData(login = "login", password = "password", repeatPassword = "wrongPassword", result = false),
            RegisterData(login = "login", password = "password", repeatPassword = "password", result = true),
        )
    }

    private val registerUseCase: RegisterUseCase = mockk {
        coEvery { this@mockk.invoke(AuthEntities.AUTH_ENTITY) } returns AuthEntities.SUCCESS_REGISTER_STATUS
    }
    private val router: Router = mockk()
    private val fieldsObserver: Observer<Boolean> = mockk(relaxed = true)

    @Test
    fun `validate fields EXPECT correct result`() {
        val viewModel = RegistrationViewModel(
            registerUseCase = registerUseCase,
            router = router
        )
        viewModel.fieldsState.observeForever(fieldsObserver)

        viewModel.validateFields(
            login = registerData.login,
            password = registerData.password,
            repeatPassword = registerData.repeatPassword
        )

        verify {
            fieldsObserver.onChanged(registerData.result)
        }
    }


}